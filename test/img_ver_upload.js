$(document).ready(function (e) {

// Função para visualizar imagem após validação
    $(function () {
        $("#file").change(function () {
            $("#msg").empty(); // Para remover a mensagem de erro anterior
            var file = this.files[0];
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
            {
                $('#previewing').attr('src', '{caminho}assets/images/user/t2_{fotoUse}');
                $("#msg").html("<p>Please Select A valid Image File</p>"
                        + "<h4>Note</h4>"
                        + "<span>Only jpeg, jpg and png Images type allowed</span>");
                return false;
            } else{
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
        function imageIsLoaded(e) {
            $('#previewing').attr('src', e.target.result);
        };
    });

});