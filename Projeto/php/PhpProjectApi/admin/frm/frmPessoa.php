<?php
//config e class padrao
require_once("../../config.php");

//config Padrão api
$Data = new Data();

use _lib\raelgc\view\Template;

$tpl = new Template("../view/frmPessoa.html");
$tpl->addFile("head", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");

//variavel Principal
$caminho = "../";
$msgText = "";
$msg = Request::Do_GET("msg", "&nbsp; &nbsp; &nbsp;");

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";

//valida dizendo que só o nivel 4 E 5 pode acessa
//$nivelR = array(0 => "4", 1 => "5");
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

//ConfigHTML
require '' . $caminho . 'includes/Config.php';

//class a ser utilizardas da page
$obj = new Pessoa();
$objE = new Endereco();
$objEP = new EnderecoPessoa();
$dao = new DaoPessoa();
$DaoE = new DaoEndereco();
$DaoEV = new DaoEnderecoPessoa();

//variaveis
$page = "";
$op = Request::Do_GET("op", 0);
$id = Request::Do_GET("id", 0);
$tipo = Request::Do_GET("tipo", 0);

// PRA DATA EM RELAÇÃO Browser
$ServiceGET->Get_NavegadorSO();

if ($id >= 1):
   $obj->setIdPessoa($id);
   $obj = $dao->selecionar($obj);

   $dataM = $Data->data_to_form($obj->getDataNascimentoPessoa(), $ServiceGET->getBrowserID());
   $obj->setDataNascimentoPessoa($dataM);

   $tpl->P = $obj;

   $objEP->setIDPESSOA($id);
   $objEP = $DaoEV->selecionar($objEP);
   $idendereco = ($objEP) ? (int) $objEP->getID() : (int) 0;

   $objE->setId($idendereco);
   $objE = $DaoE->selecionar($objE);

   $tpl->E = $objE;
//else:
//    $tpl->V = $obj;
endif;

//Arrays pra campos Select
$generos = $DaoArray->ArrayGenero();
$estadocivil = $DaoArray->ArrayEstadoCivil();
//$Flag = $DadosArray->ArrayFlag();

if ($tipo == 0):
   $page = "";
endif;
if ($tipo == 1):
   $page = "Paciente/Solicitante";
endif;
if ($tipo == 2):
   $page = "Usuários";
endif;
if ($tipo == 3):
   $page = "Médico"; //
endif;

if ($op == 0) {
   foreach ($generos as $n) {//
      $tpl->IDG = $n["id"];
      $tpl->VALUEG = $n["value"];
      $tpl->block("BLOCK_G");
   };
   foreach ($estadocivil as $n) {//
      $tpl->IDEC = $n["id"];
      $tpl->VALUEEC = $n["value"];
      $tpl->block("BLOCK_EC");
   };

   if ($ServiceGET->getBrowserID() != 2):
      $tpl->block("CAMP_DATA");
   else:
      $tpl->clear("CAMP_DATA");
   endif;
   if ($ServiceGET->getBrowserID() == 2):
      $tpl->block("CAMP_DATA2");
   else:
      $tpl->clear("CAMP_DATA2");
   endif;

   $tpl->page = "Cadastro de " . $page . "";
   $tpl->block("BLOCK_CADASTRAR");
} else {
   $tpl->clear("BLOCK_CADASTRAR");
}

//$tpl->SELECTED = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
if ($op == 1) {
   foreach ($generos as $n) {//
      $tpl->IDGa = $n["id"];
      $tpl->VALUEGa = $n["value"];
      if ($n["id"] == $obj->getIdGenero()):
         $tpl->SELECTEDGa = "selected";
      else:
         $tpl->clear("SELECTEDGa");
      endif;
      $tpl->block("BLOCK_Ga");
   };
   foreach ($estadocivil as $n) {//
      $tpl->IDECa = $n["id"];
      $tpl->VALUEECa = $n["value"];
      if ($n["id"] == $obj->getIdEstadoCivil()):
         $tpl->SELECTEDECa = "selected";
      else:
         $tpl->clear("SELECTEDECa");
      endif;
      $tpl->block("BLOCK_ECa");
   };

   if ($ServiceGET->getBrowserID() != 2):
      $tpl->block("CAMP_DATAa");
//    $tpl->DATA_ATUAL = $Data->data_atual_en();
   else:
      $tpl->clear("CAMP_DATAa");
   endif;
   if ($ServiceGET->getBrowserID() == 2):
      $tpl->block("CAMP_DATAa2");
   else:
      $tpl->clear("CAMP_DATAa2");
   endif;

   $tpl->page = "Alterar de " . $page . "";
   $tpl->block("BLOCK_AlTERAR");
} else {
   $tpl->clear("BLOCK_AlTERAR");
}
if ($op == 2) {
   foreach ($generos as $n) {//
      $tpl->IDGv = $n["id"];
      $tpl->VALUEGv = $n["value"];
      if ($n["id"] == $obj->getIdGenero()):
         $tpl->SELECTEDGv = "selected";
      else:
         $tpl->clear("SELECTEDGv");
      endif;
      $tpl->block("BLOCK_Gv");
   };
   foreach ($estadocivil as $n) {//
      $tpl->IDECv = $n["id"];
      $tpl->VALUEECv = $n["value"];
      if ($n["id"] == $obj->getIdEstadoCivil()):
         $tpl->SELECTEDECv = "selected";
      else:
         $tpl->clear("SELECTEDECv");
      endif;
      $tpl->block("BLOCK_ECv");
   };

   $tpl->disabled = "disabled";
   $tpl->page = "Visualizando " . $page . "";
   $tpl->block("BLOCK_VISUALIZAR");
} else {
   $tpl->clear("BLOCK_VISUALIZAR");
}

$tpl->id = $id;
$tpl->tipo = $tipo;

$tpl->show();
