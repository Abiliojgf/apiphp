<?php
//config e class padrao
require_once("../../config.php");

/* config Padrão api */
//$Service->Display_errors();

use _lib\raelgc\view\Template;

$tpl = new Template("../view/frmUsuario.html");
$tpl->addFile("head", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");

/* variavel Principal */
$caminho = "../";
$msgText = "";
$msg = Request::Do_GET("msg", "&nbsp; &nbsp; &nbsp;");
$op = Request::Do_GET("op", NULL);
$id = Request::Do_GET("id", NULL);

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";

/* valida dizendo que só o nivel 4 E 5 pode acessa */
//$nivelR = array(0 => "4", 1 => "5");
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';
//ConfigHTML
require '' . $caminho . 'includes/Config.php';

/* Arrays para os Select */
$cargos = $DaoArray->ArrayCargos();
$status = $DaoArray->ArrayStatus_Usuario();
$niveis = $DaoArray->ArrayNivel_Usuario();

/* class a ser utilizardas da page */
$dao = new DaoUsuario();
$daoUI = new DaoUsuarioInterno();

//CRIANDO UM OBEJTO usuariointerno
$UI = new UsuarioInterno(0, 0);

$obj = new Usuario();
if ($id >= 1):
   $obj->setId($id);
   $obj = $dao->selecionar($obj);
   $tpl->U = $obj;

   $UI = $daoUI->selecionarPeloUser($obj);
endif;

/**/
if ($op == 0) {
   $tpl->page = "Alterar Imagem do Usuário";
   $tpl->block("BLOCK_AI");
} else {
   $tpl->clear("BLOCK_AI");
}
if ($op == 1) {

   foreach ($cargos as $n) {//
      $tpl->IDNCSC = $n["id"];
      $tpl->VALUENCSC = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
      if ($UI->getID_CARGOS() == $n["id"])
         $tpl->SELECTEDNCSC = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
      else
         $tpl->clear("SELECTEDNCSC");
      $tpl->block("BLOCK_CSC");
   };
   foreach ($status as $n) {//
      $tpl->IDNSSC = $n["id"];
      $tpl->VALUENSSC = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
      if ($obj->getStatus() == $n["id"])
         $tpl->SELECTEDNSSC = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
      else
         $tpl->clear("SELECTEDNSSC");
      $tpl->block("BLOCK_SSC");
   };
   foreach ($niveis as $n) {//
      $tpl->IDNNSC = $n["id"];
      $tpl->VALUENNSC = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
      if ($obj->getNivel() == $n["id"])
         $tpl->SELECTEDNNSC = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
      else
         $tpl->clear("SELECTEDNNSC");
      $tpl->block("BLOCK_NSC");
   };

   $tpl->disabled = "disabled";
   $tpl->page = "Sua Conta";
   $tpl->block("BLOCK_SC");
} else {
   $tpl->clear("BLOCK_SC");
}
if ($op == 2) {
   $tpl->page = "Alterar Senha";
   $tpl->block("BLOCK_AS");
} else {
   $tpl->clear("BLOCK_AS");
}

if ($msg == 1) {
   $tpl->msg = '<div class="alert alert-success  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Operação Realizada com Suceesso.</div>';
}
if ($msg == 2) {
   $tpl->msg = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Erro ao realizar essa Operação.</div>';
}
if ($msg == 3) {
   $tpl->msg = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Senha Não conresponde!</div>';
}
if ($msg == 4) {
   $tpl->msg = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Senha incorreta!</div>';
}

$tpl->show();
