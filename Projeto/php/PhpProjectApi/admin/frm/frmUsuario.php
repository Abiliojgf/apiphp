<?php
//config e class padrao
require_once("../../config.php");

use _lib\raelgc\view\Template;
$tpl = new Template("../view/frmUsuario.html");
$tpl->addFile("head", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");

/* variavel Principal */
$caminho = "../";
$msgText = "";
$msg = Request::Do_GET("msg", "&nbsp; &nbsp; &nbsp;");

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";

/* valida dizendo que só o nivel 4 pode acessa */
$nivelR = array(0 => "4", 1 => "5");
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

/* ConfigHTML */
require '' . $caminho . 'includes/Config.php';

/* class a ser utilizardas da page */
$dao = new DaoUsuario();
$daoUI = new DaoUsuarioInterno();

/* Arrays pra campos Select */
$cargos = $DaoArray->ArrayCargos();
$status = $DaoArray->ArrayStatus_Usuario();
$niveis = $DaoArray->ArrayNivel_Usuario();

/* variaveis */
$op = Request::Do_GET("op", 0);
$id = Request::Do_GET("id", 0);

$tpl->id = $id;

/* CRIANDO UM OBEJTO usuariointerno */
$UI = new UsuarioInterno(0, 0);

$obj = new Usuario();
if ($id >= 1):
    $obj->setId($id);
    $obj = $dao->selecionar($obj);
    $tpl->U = $obj;
    $UI = $daoUI->selecionarPeloUser($obj);
endif;

if ($op == 0) :
    foreach ($cargos as $n) :
        $tpl->IDNC = $n["id"];
        $tpl->VALUENC = $n["value"];
        $tpl->block("BLOCK_C");
    endforeach;
    foreach ($status as $n) :
        $tpl->IDNS = $n["id"];
        $tpl->VALUENS = $n["value"];
        $tpl->block("BLOCK_S");
    endforeach;
    foreach ($niveis as $n) :
        $tpl->IDNN = $n["id"];
        $tpl->VALUENN = $n["value"];
        $tpl->block("BLOCK_N");
    endforeach;
    if ($nivel == $nivelADM[0] || $nivel == $nivelADM[1]):
        $tpl->block("BLOCK_adm");
    else:
        $tpl->clear("BLOCK_S");
        $tpl->clear("BLOCK_N");
        $tpl->clear("BLOCK_adm");
    endif;
    $tpl->page = "Cadastro de Usuário";
    $tpl->block("BLOCK_CADASTRAR");
else :
    $tpl->clear("BLOCK_CADASTRAR");
endif;

if ($op == 1) :
    foreach ($cargos as $n) :
        $tpl->IDNCa = $n["id"];
        $tpl->VALUENCa = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
        if ($UI->getID_CARGOS() == $n["id"])
            $tpl->SELECTEDNCa = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
        else
            $tpl->clear("SELECTEDNCa");
        $tpl->block("BLOCK_Ca");
    endforeach;
    foreach ($status as $n) :
        $tpl->IDNSa = $n["id"];
        $tpl->VALUENSa = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
        if ($obj->getStatus() == $n["id"])
            $tpl->SELECTEDNSa = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
        else
            $tpl->clear("SELECTEDNSa");
        $tpl->block("BLOCK_Sa");
    endforeach;

    foreach ($niveis as $n) :
        $tpl->IDNNa = $n["id"];
        $tpl->VALUENNa = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
        if ($obj->getNivel() == $n["id"])
            $tpl->SELECTEDNNa = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
        else
            $tpl->clear("SELECTEDNNa");
        $tpl->block("BLOCK_Na");
    endforeach;
    if ($nivel == $nivelADM[0] || $nivel == $nivelADM[1]):
        $tpl->block("BLOCK_admA");
    else:
        $tpl->clear("BLOCK_Sa");
        $tpl->clear("BLOCK_Na");
        $tpl->clear("BLOCK_admA");
    endif;

    $tpl->page = "Alterar de Usuário";
    $tpl->block("BLOCK_AlTERAR");
else:
    $tpl->clear("BLOCK_AlTERAR");
endif;
if ($op == 2) :
    foreach ($cargos as $n) :
        $tpl->IDNCv = $n["id"];
        $tpl->VALUENCv = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
        if ($UI->getID_CARGOS() == $n["id"])
            $tpl->SELECTEDNCv = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
        else
            $tpl->clear("SELECTEDNCv");
        $tpl->block("BLOCK_Cv");
    endforeach;
    foreach ($status as $n) :
        $tpl->IDNSv = $n["id"];
        $tpl->VALUENSv = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
        if ($obj->getStatus() == $n["id"])
            $tpl->SELECTEDNSv = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
        else
            $tpl->clear("SELECTEDNSv");
        $tpl->block("BLOCK_Sv");
    endforeach;
    foreach ($niveis as $n) :
        $tpl->IDNNv = $n["id"];
        $tpl->VALUENNv = $n["value"];
// Vendo se a opção atual deve ter o atributo "selected"
        if ($obj->getNivel() == $n["id"])
            $tpl->SELECTEDNNv = "selected"; // Caso esta não seja a opção atual, limpamos o valor da variável SELECTED
        else
            $tpl->clear("SELECTEDNNv");
        $tpl->block("BLOCK_Nv");
    endforeach;
    $tpl->disabled = "disabled";
    $tpl->page = "Visualizando Usuário";
    $tpl->block("BLOCK_VISUALIZAR");
else :
    $tpl->clear("BLOCK_VISUALIZAR");
endif;

//$PERMIT = FALSE;
//if ($nivel == $obj->getNivel() || $obj->getNivel() <= 3 || $nivel == 5):
//    $PERMIT = true;
//endif;
//if (!$PERMIT):
// header("Location: ../lst/lstUsuario.php?msg=403");
//endif;

$tpl->show();
