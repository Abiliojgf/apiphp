<?php
//config e class padrao
require_once("../../config.php");

use _lib\raelgc\view\Template;

$tpl = new Template("../view/frmCargo.html");
$tpl->addFile("head", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");

//variavel Principal
$caminho = "../";
$msgText = "";
$msg = Request::Do_GET("msg", "&nbsp; &nbsp; &nbsp;");

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";


//valida dizendo que só o nivel 4 E 5 pode acessa
//$nivelR = array(0 => "4", 1 => "5");
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

//ConfigHTML
require '' . $caminho . 'includes/Config.php';

//class a ser utilizardas da page
$obj = new Cargo();
$dao = new DaoCargo();

//variaveis
$op = Request::Do_GET("op", 0);
$id = Request::Do_GET("id", 0);

$tpl->id = $id;

if ($id >= 1):
   $obj->setIDCARGOS($id);
   $tpl->C = $dao->selecionar($obj);
else:
   $tpl->C = $obj;
endif;

if ($op == 0) {
   $tpl->page = "Cadastro de Cargo";
   $tpl->block("BLOCK_CADASTRAR");
} else {
   $tpl->clear("BLOCK_CADASTRAR");
}
if ($op == 1) {
   $tpl->page = "Alterar de Cargo";
   $tpl->block("BLOCK_AlTERAR");
} else {
   $tpl->clear("BLOCK_AlTERAR");
}
if ($op == 2) {
   $tpl->disabled = "disabled";
   $tpl->page = "Visualizando Cargo";
   $tpl->block("BLOCK_VISUALIZAR");
} else {
   $tpl->clear("BLOCK_VISUALIZAR");
}

$tpl->show();
