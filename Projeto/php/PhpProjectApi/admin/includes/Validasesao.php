<?php

//abre o buffer para inicializar os cookies
ob_start();
//inicializa a sessao
session_start();

//class
$DaoArray = new DaoArrayGeneric(); 

$ServiceGET->GETNavegador();
$niveis = $DaoArray->ArrayNivel_Usuario();

$usuid = Request::Do_SESSION("idusu" . $Service->getNameSESSION() . "", null);
$UrlLink = "" . $link . "login.php?msg=9";
// Verifica se existe os dados da sessão de login
if (!isset($_SESSION['idusu' . $Service->getNameSESSION() . ''])):
    header("Location: " . $UrlLink . "");
    exit;
else:

    $usu = new Usuario();
    $dao = new DaoUsuario();
    $usu->setId($usuid);
    $USUlogado = $dao->selecionar($usu);

    $tpl->INFO_APP = "Database: " . $Service->getNameSESSION() . " No " . $ServiceGET->getBrowserName() . "";
    $tpl->nomeUse = $USUlogado->getNome();
    $tpl->idUse = $USUlogado->getId();
    $nivel = $USUlogado->getNivel();
    $fotoUsulog = $USUlogado->getFoto();

    foreach ($niveis as $n):
        if ($nivel == $n["id"]):
            $tpl->nivelUse = $n["value"];
        endif;
    endforeach;

    //PARA MENU
    $nivelADM = array(0 => 4, 1 => 5);
    //aqui validar o acesse a funções do menu Adm
    if ($nivel == $nivelADM[0] || $nivel == $nivelADM[1]):
        $tpl->block("BLOCK_ADMIN");
    else:
        $tpl->clear("BLOCK_ADMIN");
    endif;

    //aqui começa a validação da pagina
    if (isset($nivelR)):
        if (empty($nivelR)):
        else:
            $linkParametro = "{$link}index.php?msg=505";
            $validarPage = new ValidarAcesso();
            $validarPage->validarAcessoPage($nivelR, $linkParametro, $nivel);
        endif;
    else:
    endif;

    //Validar foto do USER LOGADO
    if (file_exists($link . "assets/images/user/t1_" . $fotoUsulog) && file_exists($link . "assets/images/user/t2_" . $fotoUsulog)):
        $tpl->fotoUse = $fotoUsulog;
    else:
        $tpl->fotoUse = "user.png";
    endif;

endif;
