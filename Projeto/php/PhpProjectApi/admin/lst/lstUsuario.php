<?php
//config e class padrao
require_once("../../config.php");

//class a ser utilizardas
$dao = new DaoUsuario();

use _lib\raelgc\view\Template;

$tpl = new Template("../view/lstUsuario.html");
$tpl->addFile("head", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");
$tpl->addFile("ModalOpcoes", "../view/Include/ModalOpcoes.html");

//variavel Principal
$caminho = "../";
$msgText = "";
$TextMsg = "";
$msg = Request::Do_GET("msg", "&nbsp; &nbsp; &nbsp;");
$op = Request::Do_GET("op", 0);

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";

//valida dizendo que só o nivel 4 pode acessa
$nivelR = array(0 => "4", 1 => "5");
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

//ConfigHTML
require '' . $caminho . 'includes/Config.php';

if ($msg == 1) {
   $TextMsg = '<div id="msg"><div class="alert alert-success  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Operação Realizada com Suceesso.</div></div>';
}
if ($msg == 2) {
   $TextMsg = '<div id="msg"><div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Erro ao realizar essa Operação.</div></div>';
}
if ($msg == 403) {
   $TextMsg = '<div id="msg"><div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Sem Permissão para realizar essa Operação.</div></div>';
}

if ($op == 0):
   $tpl->clear("ModalOpcoes");
   $tpl->dados = $dao->Listar();
else:
   $tpl->nomepage = "Usuario";
   $tpl->TIPO = 0;
   $tpl->dados = $dao->ListarToFone();
endif;

$tpl->msg = $TextMsg;
$tpl->OP = $op;
$tpl->show();
