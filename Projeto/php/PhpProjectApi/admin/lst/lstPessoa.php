<?php
//config e class padrao
require_once("../../config.php");

use _lib\raelgc\view\Template;

$tpl = new Template("../view/lstPessoa.html");
$tpl->addFile("head", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");

//variavel Principal
$caminho = "../";
$msgText = "";
$msg = Request::Do_GET("msg", "&nbsp; &nbsp; &nbsp;");

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";

//valida dizendo que só o nivel 4 pode acessa
//$nivelR = array(0 => "4", 1 => "5");
//$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

//ConfigHTML
require '' . $caminho . 'includes/Config.php';

$page = "";
$TextMsg = "";

$tipo = Request::Do_GET("tipo", 0);
$op = Request::Do_GET("op", 0);

if ($tipo == 0)
   $page = "";
if ($tipo == 1)
   $page = "Paciente/Solicitante";
if ($tipo == 2)
   $page = "Usuários";
if ($tipo == 3)
   $page = "Médico";

if ($msg == 1) {
   $TextMsg = '<div id="msg"><div class="alert alert-success  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Operação Realizada com Suceesso.</div></div>';
}
if ($msg == 2) {
   $TextMsg = '<div id="msg"><div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Erro ao realizar essa Operação.</div></div>';
}
if ($msg == 502) {
   $TextMsg = '<div id="msg"><div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Estamos implementando essa Operação ou erro interno.</div></div>';
}

$dao = new DaoPessoa();

if ($op == 0):
   $tpl->dados = $dao->Listar($tipo);
else:
   $tpl->dados = $dao->ListarToFone($tipo);
endif;

$tpl->msg = $TextMsg;
$tpl->OP = $op;
//$tpl->TIPO = $tipo;
$tpl->page = $page;
$tpl->show();
