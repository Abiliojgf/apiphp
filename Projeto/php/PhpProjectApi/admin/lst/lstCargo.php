<?php
//config e class padrao
require_once("../../config.php");

use _lib\raelgc\view\Template;

$tpl = new Template("../view/lstCargo.html");
$tpl->addFile("head", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");

//variavel Principal
$caminho = "../";
$msgText = "";
$msg = Request::Do_GET("msg", 0);

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";

//valida dizendo que só o nivel 4 E 5 pode acessa
//$nivelR = array(0 => "4", 1 => "5");
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

//ConfigHTML
require '' . $caminho . 'includes/Config.php';

if ($msg == 1) {
   $msgText = '<div id="msg"><div class="alert alert-success  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Operação Realizada com Suceesso.</div></div>';
}
if ($msg == 2) {
   $msgText = '<div id="msg"><div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Erro ao realizar essa Operação.</div></div>';
}
if ($msg == 502) {
   $msgText = '<div id="msg"><div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Atenção!</strong> Estamos implementando essa Operação.</div></div>';
}

//class a ser utilizardas
$dao = new DaoCargo();
$tpl->dados = $dao->Listar();

$tpl->msg = $msgText;
$tpl->show();
