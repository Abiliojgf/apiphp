
Refet(1, 8000);
var logMsg = "";

$(document).ready(function () {

//    var isOK = false;
    var tipo = parseInt($("#tipo").val());
    var op = parseInt($("#op").val());
    var tipoText = "";
    var link = "";


    if (typeof tipo === "undefined") {
        tipo = 0;
        console.log("tipo é undefined"); // Não será mostrado
    } else {
        if (tipo > 0) {
            tipoText = "&tipo=" + tipo + "";
        }
//        console.log(tipoText); // Será mostrado
    }

    //resolução 'real' navegador
    var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;

    if (width <= 727) {
        $(".column_hidden").remove();
        if (op === 0) {
            link = "?op=1" + tipoText + "";
            window.location.href = link;
        }
        console.log("FONE");
    } else {
        //dataTable list
        $('#dataTablesList').DataTable({
            "language": {
                "url": "../assets/langs/Datatables/Portuguese-Brasil.json"
            },
//            "pagingType": "full_numbers",
//            "responsive": true,
            stateSave: true,
            "processing": true,
            "serverSide": false
        });
    }

    var nomepage = $("#nomepage").val();
    var urlPADRAO = "../../Controle/cad_" + nomepage + ".php?acao=";

    //click blur mouseover dblclick change
    $(".Opids").on("click", function () {
        var valorIds = $(this).val();
        $('#valorIds').val(valorIds);
        $('.ModalOpcoes').modal('show');
    });

    $(".Opcoes").on("click", function () {
        //indentifique o elemento clicada
        var self = $(this);
        //procura o valo id pelo elemento indentifique
        var valorIds = self.closest("tr").find(".IDRa").val();
        $('#valorIds').val(valorIds);
        $('.ModalOpcoes').modal('show');
    });

    $("#OpEditar").on("click", function () {
        var valorIds = $('#valorIds').val();
        var acao = "a";
        window.location.href = "" + urlPADRAO + "" + acao + "&id=" + valorIds + "" + tipoText + "";
    });

    $("#OpVisualizar").on("click", function () {
        var valorIds = $('#valorIds').val();
        var acao = "v";
        window.location.href = "" + urlPADRAO + "" + acao + "&id=" + valorIds + "" + tipoText + "";
    });

    $("#OpRemover").on("click", function () {
        var decisao = confirm("Deseja realmente Excluir?");
        if (decisao) {
            var valorIds = $('#valorIds').val();
            var acao = "e";
            window.location.href = "" + urlPADRAO + "" + acao + "&id=" + valorIds + "" + tipoText + "";
        } else {
            return false;
        }
    });

});
