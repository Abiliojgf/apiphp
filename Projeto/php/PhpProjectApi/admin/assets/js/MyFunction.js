// função em jquery pra quadno aperta F5
function ApertaF5(){
   $(document).ready(function(){
      $(document).keydown(function(event) {
          var k = event.keyCode
           if(k == 116){
              $.post() // funcao post jquery para outra pagina que fara o ajax
          }
      })
   });
}

function Carregado() {
    $("#msg").html('<h3 class="text-info">Carregado!</h3>');
}
function limpar() {
    $("#msg").empty();
}

function Refet(msg, tempo) {
    var tempoFull = 4000;
    if (tempo != null) {
        tempoFull = tempo;
    }
    if (msg === 0) {
        setTimeout('Carregado()', 4000);
        setTimeout('limpar()', 6000);
    }
    if (msg > 0) {
        setTimeout('limpar()', tempoFull);
    }
}
function ValiForm() {
    var decisao = confirm("Os Dados estão Corretos?");
    if (decisao) {
        return false;
    } else {
        return false;
    }
}

function ValiExc() {
    var decisao = confirm("Deseja realmente Excluir?");
    if (decisao) {
        return false;
    } else {
        return false;
    }
}
function checar_caps_lock(ev) {
    var e = ev || window.event;
    codigo_tecla = e.keyCode ? e.keyCode : e.which;
    tecla_shift = e.shiftKey ? e.shiftKey : ((codigo_tecla == 16) ? true : false);
    if (((codigo_tecla >= 65 && codigo_tecla <= 90) && !tecla_shift) || ((codigo_tecla >= 97 && codigo_tecla <= 122) && tecla_shift)) {
        $('#aviso_caps_lock').html('<div class="text-center alert-info">Atenção: O Caps Lock esta ativado</div>');
    } else {
        $('#aviso_caps_lock').empty();
    }
}

function MostrarDIV(params) {
    if (params == 0)
        $("#CampDIV").show();
    if (params == 1)
        $("#CampDIVI").show();
    if (params == 2)
        $("#CampDIVII").show();
    if (params == 3)
        $("#CampDIVIII").show();
}
function EsconderDIV(params) {
    if (params == 0)
        $("#CampDIV").hide();
    if (params == 1)
        $("#CampDIVI").hide();
    if (params == 2)
        $("#CampDIVII").hide();
    if (params == 3)
        $("#CampDIVIII").hide();
}

function enableCPF() {
    $("#cpf").prop("readonly", false);
    $("#cnpj").prop("readonly", true);
}

function enableCNPJ() {
    $("#cpf").prop("readonly", true);
    $("#cnpj").prop("readonly", false);
}