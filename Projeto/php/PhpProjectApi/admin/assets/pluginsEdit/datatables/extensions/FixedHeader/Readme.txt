# FixedHeader
Às vezes, pode ser útil garantir que os títulos das colunas permaneçam sempre visíveis 
em uma tabela, mesmo quando um usuário rola para baixo em uma tabela. 
O plug-in FixedHeader para DataTables flutuará o elemento 'thead' acima da tabela em 
todos os momentos para ajudar a resolver este problema. 
Os títulos das colunas também permanecem clicáveis para executar a classificação.
Os principais recursos incluem:

* Corrigir o cabeçalho para o topo da janela
* Possibilidade de corrigir o rodapé e as colunas esquerda / direita também
* Opções de ordenação do índice z


# Instalação

Para usar o FixedHeader, primeiro faça o download de DataTables (http://datatables.net/download) 
e coloque o pacote FixedHeader descompactado no diretório `extensions` 
=no pacote DataTables. Isso permitirá que as páginas nos exemplos funcionem corretamente. 
Para ver os exemplos em execução, abra o diretório `examples` no seu navegador.


# Uso básico

FixedHeader é inicializado usando o objeto `$.fn.dataTable.FixedHeader()`. Por exemplo:

```js
$(document).ready( function () {
    var table = $('#example').dataTable();
    new $.fn.dataTable.FixedHeader( table );
} );
```


# Documentation / support

* Documentation: http://datatables.net/extensions/FixedHeader/
* DataTables support forums: http://datatables.net/forums


# GitHub

Se você gosta de se envolver com o desenvolvimento do FixedHeader e ajudar a torná-lo melhor,
Por favor consulte o seu site GitHub : https://github.com/DataTables/FixedHeader

