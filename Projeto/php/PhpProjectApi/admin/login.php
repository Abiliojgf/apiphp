<?php
ini_set('default_charset', 'UTF-8');
error_reporting(E_STRICT);
session_start();

require_once("../vendor/autoload.php");

//class Padrão api
$Service = new ConfigServerPHP();
$ServiceGET = new GetInfoSettings();
$String = new ToString(null);
$DaoArray = new DaoArrayGeneric();
$info = new GetInfoSettings();

$usuid = $_SESSION['idusu' . $Service->getNameSESSION() . ''];
if ($usuid != null):
   header("location: ./index.php");
endif;

use _lib\raelgc\view\Template;

$tpl = new Template("view/login.html");

//variavel Principal
$msgText = "";
$msg = Request::Do_GET("msg", 0);
$op = Request::Do_GET("op", 0);

//Aviso / Informação
if ($msg == 0) {
   $msgText = '';
}
if ($msg == 1) {
   $msgText = '<div id="msg"><div  class="alert-warning">Aviso: Login invalido!</div></div>';
}
if ($msg == 2) {
   $msgText = '<div id="msg"><div  class="alert-warning">Aviso: Sua Senha é invalida!</div></div>';
}
if ($msg == 3) {//alert-info alert-warning alert-danger alert-success
   $msgText = '<div id="msg"><div  class="alert-warning">Informação: Sem acesso temporariamente!</div></div>';
}
if ($msg == 4) {//alert-info alert-warning alert-danger alert-success
   $msgText = '<div id="msg"><div  class="alert-info">Informação: Usuário sem acesso temporariamente, por motivo de Segurança!</div></div>';
}
if ($msg == 5) {//alert-info alert-warning alert-danger alert-success
   $msgText = '<div id="msg"><div  class="alert-warning">Aviso: Sessão Foi Fechada!</div></div>';
}
if ($msg == 6) {//alert-info alert-warning alert-danger alert-success
   $msgText = '<div id="msg"><div  class="alert-success">Email envido! Só aguarda alguns minutos sua senha!</div></div>';
}
if ($msg == 7) {//alert-info alert-warning alert-danger alert-success
   $msgText = '<div id="msg"><div  class="alert-info">Informação: Usuário Cancelado, por motivo de Segurança!</div></div>';
}
if ($msg == 8) {//alert-info alert-warning alert-danger alert-success
   $msgText = '<div id="msg"><div  class="alert-danger">Erro: Não existe Usuário com esse Email em nosso sistema!!</div></div>';
}
if ($msg == 9) {//alert-info alert-warning alert-danger alert-success
   $msgText = '<div id="msg"><div  class="alert-info">Informação! Sessão Expirada!</div></div>';
}

if ($op == 0) {
   $tpl->block("BLOCK_L");
} else {
   $tpl->clear("BLOCK_L");
}

if ($op != 0) {
   $tpl->block("BLOCK_RS");
} else {
   $tpl->clear("BLOCK_RS");
}

$tpl->msg = $msgText;
$tpl->INFO_APP = "Database: " . $Service->getNameSESSION() . "<br/> Navegador: " . $ServiceGET->getBrowserName() . " " . $ServiceGET->getBrowserVersion() . " ";
// . " so: " . $ServiceGET->getSO()
$tpl->show();
