# PHP-files-manipulation-class
## Este pacote pode manipular arquivos e diretórios de várias maneiras.

Pode executar vários tipos de operações. Atualmente pode:
  1. Criar diretório
  2. Gerar cadeia aleatória
  3. Alterar permissão de arquivo
  4. Copiar arquivos ou pastas
  5. Mover arquivos e pastas
  6. Excluir arquivos e pastas
  7. Carregar arquivos com validação
  8. arquivos de leitura / gravação
  9. Definir diretório e subdiretório padrão
  10. E definir diretório para fora do lado da raiz do servidor web também.

	## Configuraiton
	Open core/init.php, você vê o seguimento

    <?php
    //Configurando o diretório de dados
    $Malik['MalikDataDir'] = 'E:/Malik/Server/';
    //configurando sub-pasta que ele cria automaticamente
    $Malik['subfolder'] = 'Malik';
    
	//classe de carga
    require_once 'classess/MalikFiles.php';

	Forneça o seu caminho do diretório principal
    $Malik['MalikDataDir'] = 'E:/Malik/Server/';

	Fornecer um caminho sub dir
    $Malik['subfolder'] = 'Malik';

--------------------------------------------------------------------------------
//loading require files
require_once 'core/init.php';
//creating objects
$files = new Files;
//check if form submit
if(isset($_POST['submit'])){
	//passing $_FILES['value of name attribute']
	// target if you want upload in main data folder leave as / otherwise apply if folder not exists is create first
	//filetype type of file image,media,zip,docs supported for more info see line 224 of class file
	$fileName = $files->FileUpload(['file'=>$_FILES['file'],'target'=>'images','filetype'=>'image']);
	echo $fileName;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>PHP File upload</title>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
		<input type="file" name="file">
		<br>
		<input type="submit" name="submit">
	</form>
</body>
</html>
--------------------------------------------------------------------------------