<?php
/**
 * Description of Genero
 *
 * @author AJ
 */
class Genero {
    
    private $ID_GENERO;
    private $TIPO_GENERO;
    
    function __construct() {        
    }
    
    function getID_GENERO() {
        return $this->ID_GENERO;
    }

    function getTIPO_GENERO() {
        return $this->TIPO_GENERO;
    }

    function setID_GENERO($ID_GENERO) {
        $this->ID_GENERO = $ID_GENERO;
    }

    function setTIPO_GENERO($TIPO_GENERO) {
        $this->TIPO_GENERO = $TIPO_GENERO;
    }

}
