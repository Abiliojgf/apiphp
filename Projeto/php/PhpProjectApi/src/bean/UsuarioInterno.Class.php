<?php

class UsuarioInterno {

   private $ID_CARGOS;
   private $ID_USUARIO;

   function __construct($ID_CARGOS, $ID_USUARIO) {
      $this->ID_CARGOS = $ID_CARGOS;
      $this->ID_USUARIO = $ID_USUARIO;
   }

   function getID_CARGOS() {
      return $this->ID_CARGOS;
   }

   function getID_USUARIO() {
      return $this->ID_USUARIO;
   }

}
