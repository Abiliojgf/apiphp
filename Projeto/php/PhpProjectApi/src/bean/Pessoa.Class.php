<?php

class Pessoa {

    private $idPessoa;
    private $idGenero;
    private $idEstadoCivil;
    private $nomePessoa;
    private $cpfPessoa;
    private $rgPessoa;
    private $orgaoPessoa;
    private $telefonePessoa;
    private $celularPessoa;
    private $naturalidadePessoa;
    private $complementoEnd1Pessoa;
    private $dataNascimentoPessoa;
    private $dataCadastroPessoa;
    private $nsusPessoa;
    private $status;
    private $tipo;

    function construct() {        
    }
    
    function getIdPessoa() {
        return $this->idPessoa;
    }

    function getIdGenero() {
        return $this->idGenero;
    }

    function getIdEstadoCivil() {
        return $this->idEstadoCivil;
    }

    function getNomePessoa() {
        return $this->nomePessoa;
    }

    function getCpfPessoa() {
        return $this->cpfPessoa;
    }

    function getRgPessoa() {
        return $this->rgPessoa;
    }

    function getOrgaoPessoa() {
        return $this->orgaoPessoa;
    }

    function getTelefonePessoa() {
        return $this->telefonePessoa;
    }

    function getCelularPessoa() {
        return $this->celularPessoa;
    }

    function getNaturalidadePessoa() {
        return $this->naturalidadePessoa;
    }

    function getComplementoEnd1Pessoa() {
        return $this->complementoEnd1Pessoa;
    }

    function getDataNascimentoPessoa() {
        return $this->dataNascimentoPessoa;
    }

    function getDataCadastroPessoa() {
        return $this->dataCadastroPessoa;
    }

    function getNsusPessoa() {
        return $this->nsusPessoa;
    }

    function getStatus() {
        return $this->status;
    }

    function getTipo() {
        return $this->tipo;
    }

    function setIdPessoa($idPessoa) {
        $this->idPessoa = $idPessoa;
    }

    function setIdGenero($idGenero) {
        $this->idGenero = $idGenero;
    }

    function setIdEstadoCivil($idEstadoCivil) {
        $this->idEstadoCivil = $idEstadoCivil;
    }

    function setNomePessoa($nomePessoa) {
        $this->nomePessoa = $nomePessoa;
    }

    function setCpfPessoa($cpfPessoa) {
        $this->cpfPessoa = $cpfPessoa;
    }

    function setRgPessoa($rgPessoa) {
        $this->rgPessoa = $rgPessoa;
    }

    function setOrgaoPessoa($orgaoPessoa) {
        $this->orgaoPessoa = $orgaoPessoa;
    }

    function setTelefonePessoa($telefonePessoa) {
        $this->telefonePessoa = $telefonePessoa;
    }

    function setCelularPessoa($celularPessoa) {
        $this->celularPessoa = $celularPessoa;
    }

    function setNaturalidadePessoa($naturalidadePessoa) {
        $this->naturalidadePessoa = $naturalidadePessoa;
    }

    function setComplementoEnd1Pessoa($complementoEnd1Pessoa) {
        $this->complementoEnd1Pessoa = $complementoEnd1Pessoa;
    }

    function setDataNascimentoPessoa($dataNascimentoPessoa) {
        $this->dataNascimentoPessoa = $dataNascimentoPessoa;
    }

    function setDataCadastroPessoa($dataCadastroPessoa) {
        $this->dataCadastroPessoa = $dataCadastroPessoa;
    }

    function setNsusPessoa($nsusPessoa) {
        $this->nsusPessoa = $nsusPessoa;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }


}
