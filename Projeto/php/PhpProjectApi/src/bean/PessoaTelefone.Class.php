<?php
/**
 * Description of PessoaTelefone
 *
 * @author AJ
 */
class PessoaTelefone {

    private $IDPESSOA;
    private $IDCONTATOTEL;
    
    function __construct($IDPESSOA, $IDCONTATOTEL) {
        $this->IDPESSOA = $IDPESSOA;
        $this->IDCONTATOTEL = $IDCONTATOTEL;
    }

    function getIDPESSOA() {
        return $this->IDPESSOA;
    }

    function getIDCONTATOTEL() {
        return $this->IDCONTATOTEL;
    }


}
