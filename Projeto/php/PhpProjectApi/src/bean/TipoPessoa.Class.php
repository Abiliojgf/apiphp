<?php
/**
 * Description of TipoPessoa
 *
 * @author AJ
 */
class TipoPessoa {
    
    private $ID_TIPO_PESSOA;
    private $NOME_TIPO_PESSOA;
    
    function __construct($ID_TIPO_PESSOA, $NOME_TIPO_PESSOA) {
        $this->ID_TIPO_PESSOA = $ID_TIPO_PESSOA;
        $this->NOME_TIPO_PESSOA = $NOME_TIPO_PESSOA;
    }

    function getID_TIPO_PESSOA() {
        return $this->ID_TIPO_PESSOA;
    }

    function getNOME_TIPO_PESSOA() {
        return $this->NOME_TIPO_PESSOA;
    }


}
