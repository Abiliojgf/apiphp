<?php

/**
 * Description of PessoaEmail
 *
 * @author AJ
 */
class PessoaEmail {
    
    private $IDPESSOA;
    private $IDCONTATOEMAIL;
    
    function construct($IDPESSOA, $IDCONTATOEMAIL) {
        $this->IDPESSOA = $IDPESSOA;
        $this->IDCONTATOEMAIL = $IDCONTATOEMAIL;
    }
    
    function getIDPESSOA() {
        return $this->IDPESSOA;
    }

    function getIDCONTATOEMAIL() {
        return $this->IDCONTATOEMAIL;
    }

}
