<?php
/**
 * Description of ContatoEmail
 *
 * @author AJ
 */
class ContatoEmail {
    
    private $IDCONTATOEMAIL;
    private $CONTATOEMAIL;
    
    function __construct() {        
    }

    function getIDCONTATOEMAIL() {
        return $this->IDCONTATOEMAIL;
    }

    function getCONTATOEMAIL() {
        return $this->CONTATOEMAIL;
    }

    function setIDCONTATOEMAIL($IDCONTATOEMAIL) {
        $this->IDCONTATOEMAIL = $IDCONTATOEMAIL;
    }

    function setCONTATOEMAIL($CONTATOEMAIL) {
        $this->CONTATOEMAIL = $CONTATOEMAIL;
    }


}
