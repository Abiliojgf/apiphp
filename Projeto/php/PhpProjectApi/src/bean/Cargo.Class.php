<?php

class Cargo {

    private $IDCARGOS;
    private $TIPOCARGO;
    
    function __construct() {        
    }

    function getIDCARGOS() {
        return $this->IDCARGOS;
    }

    function getTIPOCARGO() {
        return $this->TIPOCARGO;
    }

    function setIDCARGOS($IDCARGOS) {
        $this->IDCARGOS = $IDCARGOS;
    }

    function setTIPOCARGO($TIPOCARGO) {
        $this->TIPOCARGO = $TIPOCARGO;
    }

}
