<?php
class Endereco {
    
    private $id;                   
    private $cidade;
    private $logradouro;
    private $bairro;
    private $cep;
    private $tpLogradouro;
    private $uf;
    private $flag;

    function __construct() {
    }
    
    function getId() {
        return $this->id;
    }

    function getCidade() {
        return $this->cidade;
    }

    function getLogradouro() {
        return $this->logradouro;
    }

    function getBairro() {
        return $this->bairro;
    }

    function getCep() {
        return $this->cep;
    }

    function getTpLogradouro() {
        return $this->tpLogradouro;
    }

    function getUf() {
        return $this->uf;
    }

    function getFlag() {
        return $this->flag;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
    }

    function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    function setCep($cep) {
        $this->cep = $cep;
    }

    function setTpLogradouro($tpLogradouro) {
        $this->tpLogradouro = $tpLogradouro;
    }

    function setUf($uf) {
        $this->uf = $uf;
    }

    function setFlag($flag) {
        $this->flag = $flag;
    }

}
