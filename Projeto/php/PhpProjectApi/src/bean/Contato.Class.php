<?php

/**
 * Description of Contato
 *
 * @author AJ
 */
class Contato {

   private $idContato;
   private $nome;
   private $email;
   private $telefone;
   private $assunto;
   private $menssagem;
   private $dataenviada;
   private $lida;

   function __construct() {

   }

   function getIdContato() {
      return $this->idContato;
   }

   function getNome() {
      return $this->nome;
   }

   function getEmail() {
      return $this->email;
   }

   function getTelefone() {
      return $this->telefone;
   }

   function getAssunto() {
      return $this->assunto;
   }

   function getMenssagem() {
      return $this->menssagem;
   }

   function getDataenviada() {
      return $this->dataenviada;
   }

   function getLida() {
      return $this->lida;
   }

   function setIdContato($idContato) {
      $this->idContato = $idContato;
   }

   function setNome($nome) {
      $this->nome = $nome;
   }

   function setEmail($email) {
      $this->email = $email;
   }

   function setTelefone($telefone) {
      $this->telefone = $telefone;
   }

   function setAssunto($assunto) {
      $this->assunto = $assunto;
   }

   function setMenssagem($menssagem) {
      $this->menssagem = $menssagem;
   }

   function setDataenviada($dataenviada) {
      $this->dataenviada = $dataenviada;
   }

   function setLida($lida) {
      $this->lida = $lida;
   }

}
