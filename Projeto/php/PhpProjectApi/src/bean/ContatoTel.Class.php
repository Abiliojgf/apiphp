<?php
/**
 * Description of contatotel
 *
 * @author AJ
 */
class ContatoTel {

    private $IDCONTATOTEL;
    private $NUMEROCONTATOTEL;
    private $TIPOCONTATOTEL;
    
    function __construct() {        
    }

    function getIDCONTATOTEL() {
        return $this->IDCONTATOTEL;
    }

    function getNUMEROCONTATOTEL() {
        return $this->NUMEROCONTATOTEL;
    }

    function getTIPOCONTATOTEL() {
        return $this->TIPOCONTATOTEL;
    }

    function setIDCONTATOTEL($IDCONTATOTEL) {
        $this->IDCONTATOTEL = $IDCONTATOTEL;
    }

    function setNUMEROCONTATOTEL($NUMEROCONTATOTEL) {
        $this->NUMEROCONTATOTEL = $NUMEROCONTATOTEL;
    }

    function setTIPOCONTATOTEL($TIPOCONTATOTEL) {
        $this->TIPOCONTATOTEL = $TIPOCONTATOTEL;
    }


}
