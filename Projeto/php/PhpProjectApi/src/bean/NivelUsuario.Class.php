<?php
/**
 * Description of NivelUsuario
 *
 * @author AJ
 */
class NivelUsuario {
    
    private $IDNIVEL;
    private $TIPONIVEL;
    
    function __construct($IDNIVEL, $TIPONIVEL) {
        $this->IDNIVEL = $IDNIVEL;
        $this->TIPONIVEL = $TIPONIVEL;
    }
    
    function getIDNIVEL() {
        return $this->IDNIVEL;
    }

    function getTIPONIVEL() {
        return $this->TIPONIVEL;
    }
 
}
