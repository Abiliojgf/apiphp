<?php
/**
 * Description of EnderecoPessoa
 *
 * @author AJ
 */
class EnderecoPessoa {
    
    private $ID;
    private $IDPESSOA;
    
    function __construct() {        
    }
    
    function getID() {
        return (int) $this->ID;
    }

    function getIDPESSOA() {
        return (int) $this->IDPESSOA;
    }

    function setID($ID) {
        $this->ID = (int) $ID;
    }

    function setIDPESSOA($IDPESSOA) {
        $this->IDPESSOA = (int) $IDPESSOA;
    }

}
