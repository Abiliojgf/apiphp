<?php
/**
 * Description of EstadoCivil
 *
 * @author AJ
 */
class EstadoCivil {

    private $IDESTADOCIVIL;
    private $TIPOESTADOCIVIL;
    
    function __construct() {        
    }

    function getIDESTADOCIVIL() {
        return $this->IDESTADOCIVIL;
    }

    function getTIPOESTADOCIVIL() {
        return $this->TIPOESTADOCIVIL;
    }

    function setIDESTADOCIVIL($IDESTADOCIVIL) {
        $this->IDESTADOCIVIL = $IDESTADOCIVIL;
    }

    function setTIPOESTADOCIVIL($TIPOESTADOCIVIL) {
        $this->TIPOESTADOCIVIL = $TIPOESTADOCIVIL;
    }


}
