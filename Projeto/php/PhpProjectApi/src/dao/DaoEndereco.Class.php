<?php

/**
 * Description of DaoEndereco
 *
 * @author AJ
 */
class DaoEndereco {

   //ENDERECO "ID", "CIDADE", "LOGRADOURO", "BAIRRO", "CEP", "TP_LOGRADOURO", "UF" ,"FLAG"
   public function inserir(Endereco $obj) {
      $dado = array("", $obj->getCidade(), $obj->getLogradouro(), $obj->getBairro(), $obj->getCep(), $obj->getTpLogradouro(), $obj->getUf(), $obj->getFlag());
      $coluna = null; //array();
      $dao = new DaoFull();
      $dao->table = "ENDERECO";
      return $dao->inserir($dado, $coluna, null);
   }

   public function Listar() {
      $camposTabelas = array();
      $nomeTabelas = array();
      $condicoes = array();
      $dao = new DaoFull();
      $dao->arrayTable = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP, $arrayTO
      $arrayDados = $dao->listar($camposTabelas, $condicoes, null, "ASC", null, null, null);
      if ($arrayDados != null) {
         $objMontaDados = new MontaDados;
         $objMontaDados->CampoData = array(0 => "");
         $objMontaDados->colunas = $camposTabelas;
         $objMontaDados->dados = $arrayDados;
         return $objMontaDados->deListar(2, "../../controller/cad_Endereco.php", 7);
      } else {
         return null;
      }
   }

   public function selecionar(Endereco $obj) {
      $camposTabelas = array("e.ID", "e.CIDADE", "e.LOGRADOURO", "e.BAIRRO", "e.CEP", "e.TP_LOGRADOURO", "e.UF", "e.FLAG");
      $nomeTabelas = array("e" => "ENDERECO");
      $condicoes = array("e.ID = " . $obj->getId() . " ");
      $dao = new DaoFull();
      $dao->arrayTable = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
      $d = $dao->selecionar($camposTabelas, $condicoes, null, null, null, null, null);
      if ($d != null) {
         $obj->setId($d->dado[0]);
         $obj->setCidade($d->dado[1]);
         $obj->setLogradouro($d->dado[2]);
         $obj->setBairro($d->dado[3]);
         $obj->setCep($d->dado[4]);
         $obj->setTpLogradouro($d->dado[5]);
         $obj->setUf($d->dado[6]);
         $obj->setFlag($d->dado[7]);
      } else {
         $obj->setId(0);
      }
      return $obj;
   }

   public function selecionar_Verificar(Endereco $obj) {
      $camposTabelas = array("e.ID", "e.CIDADE", "e.LOGRADOURO", "e.BAIRRO", "e.CEP", "e.TP_LOGRADOURO", "e.UF", "e.FLAG");
      $nomeTabelas = array("e" => "ENDERECO");
      $condicoes = array("e.ID = " . $obj->getId() . " ");
      $dao = new DaoFull();
      $dao->arrayTable = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
      $d = $dao->selecionar($camposTabelas, $condicoes, null, null, null, null, null);
      if ($d != null) {
         $obj->setId($d->dado[0]);
         $obj->setCidade($d->dado[1]);
         $obj->setLogradouro($d->dado[2]);
         $obj->setBairro($d->dado[3]);
         $obj->setCep($d->dado[4]);
         $obj->setTpLogradouro($d->dado[5]);
         $obj->setUf($d->dado[6]);
         $obj->setFlag($d->dado[7]);
         if ($obj->getCep() != "" || $obj->getLogradouro() != "" || $obj->getBairro() != "" || $obj->getCidade() != ""):
            return true;
         else:
            return false;
         endif;
      } else {
         return false;
      }
   }

   public function PegarIdpeloCEP($CEP) {
      $camposTabelas = array("e.ID");
      $nomeTabelas = array("e" => "ENDERECO");
      $condicoes = array("e.CEP =" . $CEP . ""); //,"e.FLAG=1"
      $dao = new DaoFull();
      $dao->arrayTable = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
      $d = $dao->selecionar($camposTabelas, $condicoes, "e.ID", null, 1, null, null);
      if ($d != null) {
         $Id = $d->dado[0];
      } else {
         $Id = 0;
      }
      return $Id;
   }

   public function PegarOBJpeloCEP($CEP) {
      $camposTabelas = array("e.ID", "e.CIDADE", "e.LOGRADOURO", "e.BAIRRO", "e.CEP", "e.TP_LOGRADOURO", "e.UF", "e.FLAG");
      $nomeTabelas = array("e" => "ENDERECO");
      $condicoes = array("e.CEP =" . $CEP . ""); //,"e.FLAG=1"
      $obj = new Endereco();
      $dao = new DaoFull();
      $dao->arrayTable = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
      $d = $dao->selecionar($camposTabelas, $condicoes, null, null, null, null, null);
      if ($d != null) {
         $obj->setId($d->dado[0]);
         $obj->setCidade($d->dado[1]);
         $obj->setLogradouro($d->dado[2]);
         $obj->setBairro($d->dado[3]);
         $obj->setCep($d->dado[4]);
         $obj->setTpLogradouro($d->dado[5]);
         $obj->setUf($d->dado[6]);
         $obj->setFlag($d->dado[7]);
      } else {
         $obj->setId(0);
      }
      return $obj;
   }

   public function PegarUltimoId() {
      $camposTabelas = array("e.ID");
      $nomeTabelas = array("e" => "ENDERECO");
      $condicoes = NULL;
      $dao = new DaoFull();
      $dao->arrayTable = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
      $d = $dao->selecionar($camposTabelas, $condicoes, "e.ID", "DESC", 1, null, null);
      if ($d != null) {
         $Id = $d->dado[0];
      } else {
         $Id = 0;
      }
      return $Id;
   }

   public function alterar(Endereco $obj) {
      $dado = array($obj->getCidade(), $obj->getLogradouro(), $obj->getBairro(), $obj->getCep(), $obj->getTpLogradouro(), $obj->getUf(), $obj->getFlag());
      $camposTabelas = array("CIDADE", "LOGRADOURO", "BAIRRO", "CEP", "TP_LOGRADOURO", "UF", "FLAG");
      $where = "ID = " . $obj->getId() . " ";
      $dao = new DaoFull();
      $dao->table = "ENDERECO";
      if ($dao->Atualizar($dado, $camposTabelas, $where, null)) {
         return true;
      } else {
         return false;
      }
   }

   public function fucaoAtualizarDefull($dado, $camposTabelas, $where) {
      $dao = new DaoFull();
      $dao->table = "ENDERECO";
      return $dao->Atualizar($dado, $camposTabelas, $where, null);
   }

   public function fucaoVerificarDefull($where) {
      $dao = new DaoFull();
      $dao->arrayTable = array("e" => "ENDERECO");
      return $dao->Verificar($where, null);
   }

   public function fucaoVerificarRETURNInt($where) {
      $conn = new Conexao();
      $sql = new Sql(null);
      $sql->arrayTable = array("e" => "ENDERECO");
      $sql->condicoesTabela = $where;
      $conn->sql = $sql->sqlPesquisar();
      $rusult = $conn->linhasPesquisadas("select");
      return $rusult;
   }

   public function excluir(Endereco $obj) {
      $where = array("ID = " . $obj->getId() . " ");
      $dao = new DaoFull();
      $dao->table = "ENDERECO";
      if ($dao->excluir($where, null)) {
         return true;
      } else {
         return false;
      }
   }

}
