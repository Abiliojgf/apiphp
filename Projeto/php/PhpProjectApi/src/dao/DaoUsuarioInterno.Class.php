<?php

class DaoUsuarioInterno {

   public function inserir(UsuarioInterno $objUI) {
      $dado = array($objUI->getID_CARGOS(), $objUI->getID_USUARIO());
      $coluna = array("ID_CARGOS", "ID_USUARIO");
      $dao = new DaoFull();
      $dao->table = "USUARIO_INTERNO";
      return $dao->inserir($dado, $coluna, null);
   }

   public function Listar() {
//        $camposTabelas = array();
//        $nomeTabelas = array();
//        $dao = new DaoFull();
//        $dao->table=$nomeTabelas;
//        $arrayDados = $dao->listar($camposTabelas, null, "", null);
//        if ($arrayDados != null) {
//            $objMontaDados = new MontaDados;
//            $objMontaDados->colunas = $camposTabelas;
//            $objMontaDados->dados = $arrayDados;
//            return $objMontaDados->deListar(2, "",7);
//        } else {
//            return null;
//        }
   }

   public function selecionarPeloUser(Usuario $usu) {

      $camposTabelas = array("u.ID_CARGOS", "u.ID_USUARIO");
      $nomeTabelas = array("u" => "USUARIO_INTERNO");
      $condicoes = array("u.ID_USUARIO = {$usu->getId()} ");

      $dao = new DaoFull();
      $dao->table = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
      $d = $dao->selecionar($camposTabelas, $condicoes, null, null, null, null, null);
      if ($d != null) {
         $objUI = new UsuarioInterno($d->dado[0], $d->dado[1]);
      } else {
         $objUI = new UsuarioInterno(0, 0);
      }
      return $objUI;
   }

   public function alterar(UsuarioInterno $objUI) {
      $dado = array($objUI->getID_CARGOS());
      $camposTabelas = array("ID_CARGOS");
      $where = "ID_USUARIO = {$objUI->getID_USUARIO()}";

      $dao = new DaoFull();
      $dao->table = "USUARIO_INTERNO";

      if ($dao->Atualizar($dado, $camposTabelas, $where, null)) {
         return true;
      } else {
         return false;
      }
   }

   public function fucaoAtualizarDefull($dado, $camposTabelas, $where) {
      $dao = new DaoFull();
      $dao->table = "USUARIO_INTERNO";
      return $dao->Atualizar($dado, $camposTabelas, $where, null);
   }

   public function excluir(UsuarioInterno $objUI) {
//         $where = array();
//         $dao=new DaoFull();
//         $dao->table="USUARIO_INTERNO";
//
//        if ($dao->excluir($where,null)) {
//            return true;
//        } else {
//            return false;
//        }
   }

}
