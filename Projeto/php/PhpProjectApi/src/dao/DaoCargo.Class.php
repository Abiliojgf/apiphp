<?php

class DaoCargo {
    
//  CARGOS   "ID_CARGOS","TIPO_CARGO"
    
    public function inserir(Cargo $obj) { 
        $dado = array("",$obj->getTIPOCARGO());
        $coluna =null;//array();
        $dao = new DaoFull();
        $dao->table = "CARGOS";
        return $dao->inserir($dado, $coluna,TRUE);
    }

    public function Listar() { 
        $camposTabelas = array("c.TIPO_CARGO");//,"c.ID_CARGOS" 
        $dao = new DaoFull();
        $dao->arrayTable = array("c"=>"CARGOS");
        //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP, $arrayTO
        $arrayDados = $dao->listar($camposTabelas, null, "c.TIPO_CARGO", "ASC", null, null,null);
        if ($arrayDados != null) {
            $objMontaDados = new MontaDados;
//            $objMontaDados->CampoData = array(0 => "");
            $objMontaDados->colunas = $camposTabelas;
            $objMontaDados->dados = $arrayDados;
            return $objMontaDados->deListar(1, "../../controller/cad_Cargo.php", 3, "");
        } else {
            return null;
        }
    }
 
    public function selecionar(Cargo $obj) {
        $camposTabelas = array("c.ID_CARGOS","c.TIPO_CARGO");
        $condicoes = array("ID_CARGOS = ". $obj->getIDCARGOS() ."");
        $dao = new DaoFull();
        $dao->table = array("c"=>"CARGOS");;
        //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
        $d = $dao->selecionar($camposTabelas, $condicoes, null, null, 1, null, null);
        if ($d != null) {
            $obj->setIDCARGOS($d->dado[0]);
            $obj->setTIPOCARGO($d->dado[1]);
        } else {
            $obj->setIDCARGOS(0);
        }
        return $obj;
    }

    public function alterar(Cargo $obj) {
        $dado = array($obj->getTIPOCARGO());
        $camposTabelas = array("TIPO_CARGO");
        $where = "ID_CARGOS =". $obj->getIDCARGOS() ."";
        $dao = new DaoFull();
        $dao->table = "CARGOS";
        if ($dao->Atualizar($dado, $camposTabelas, $where,TRUE)) {
            return true;
        } else {
            return false;
        }
    }

    public function fucaoAtualizarDefull($dado, $camposTabelas, $where) {
        $dao = new DaoFull();
        $dao->table = "CARGOS";
        return $dao->Atualizar($dado, $camposTabelas, $where,null);
    }

    public function fucaoVerificarDefull($where) {
        $dao = new DaoFull();
        $dao->table = array("c" => "CARGOS");
        return $dao->Verificar($where,null);
    }

    public function excluir(Cargo $obj) {
        $where = array("ID_CARGOS =". $obj->getIDCARGOS());
        $dao = new DaoFull();
        $dao->table = "CARGOS";
        if ($dao->excluir($where,TRUE)) {
            return true;
        } else {
            return false;
        }
    }

}
