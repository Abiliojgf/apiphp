<?php

/**
 * Description of DaoPessoa
 *
 * @author AJ
 */
class DaoPessoa {
/* "PESSOA" - "ID_PESSOA","ID_GENERO","ID_ESTADO_CIVIL","NOME_PESSOA","CPF_PESSOA","RG_PESSOA","ORGAO_PESSOA","TELEFONE_PESSOA","CELULAR_PESSOA",
    "NATURALIDADE_PESSOA","DATA_CADASTRO_PESSOA","DATA_NASCIMENTO_PESSOA","STATUS_PESSOA","COMPL_END1_PESSOA","N_SUS_PESSOA","TIPO_PESSOA"   */
    public function inserir(Pessoa $obj) { 
        $dado = array($obj->getIdGenero(),$obj->getIdEstadoCivil(),$obj->getNomePessoa(),$obj->getCpfPessoa(),$obj->getRgPessoa()
                ,$obj->getOrgaoPessoa(),$obj->getTelefonePessoa(),$obj->getCelularPessoa(),$obj->getNaturalidadePessoa()
                ,$obj->getDataCadastroPessoa(),$obj->getDataNascimentoPessoa(),$obj->getStatus()
                ,$obj->getComplementoEnd1Pessoa(),$obj->getNsusPessoa(),$obj->getTipo());
        $coluna = array("ID_GENERO","ID_ESTADO_CIVIL","NOME_PESSOA","CPF_PESSOA","RG_PESSOA","ORGAO_PESSOA","TELEFONE_PESSOA",
            "CELULAR_PESSOA","NATURALIDADE_PESSOA","DATA_CADASTRO_PESSOA","DATA_NASCIMENTO_PESSOA",
            "STATUS_PESSOA","COMPL_END1_PESSOA","N_SUS_PESSOA","TIPO_PESSOA");
        $dao = new DaoFull();
        $dao->table = "PESSOA";
        return $dao->inserir($dado, $coluna,null);
    }

    public function Listar($tipo) { 
        $camposTabelas = array("P.NOME_PESSOA","P.TELEFONE_PESSOA","P.CELULAR_PESSOA","P.DATA_CADASTRO_PESSOA","P.ID_PESSOA"); 
        $nomeTabelas = array("P"=>"PESSOA"); 
        $condicoes = array("P.TIPO_PESSOA = ". $tipo .""); 
        $dao = new DaoFull();
        $dao->table = $nomeTabelas;
        //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP, $arrayTO
        $arrayDados = $dao->listar($camposTabelas, $condicoes, null, "ASC", null, null,null);
        if ($arrayDados != null) {
            $objMontaDados = new MontaDados;
            $DadosArray = new DadosArray();
//            $DadosArray->SetArrayCampos(array(0=>"STATUS_PESSOA"));
//            $objMontaDados->ArrayCampos = $DadosArray->GetArrayCampos();
//            $objMontaDados->ArrayCamposValor = array(0=>$DadosArray->ArrayStatus());
            $objMontaDados->CampoData = array(0 => "DATA_CADASTRO_PESSOA");
            $objMontaDados->colunas = $camposTabelas;
            $objMontaDados->dados = $arrayDados;
            return $objMontaDados->deListar(2, "../../Controle/cad_Pessoa.php", 2, "&tipo=". $tipo ."");
        } else {
            return null;
        }
    }

    public function ListarToFone($tipo) { 
        $camposTabelas = array("P.NOME_PESSOA","P.TELEFONE_PESSOA","P.CELULAR_PESSOA","P.DATA_CADASTRO_PESSOA","P.ID_PESSOA"); 
        $nomeTabelas = array("P"=>"PESSOA"); 
        $condicoes = array("P.TIPO_PESSOA = ". $tipo .""); 
        $campoData = null;
        
        $dao = new DaoFull();
        $dao->table = $nomeTabelas;
        //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP, $arrayTO
        $arrayDados = $dao->listar($camposTabelas, $condicoes, null, "ASC", null, null,null);
        if ($arrayDados != null) {
            $objMontaDados = new MontaDados;
            $objMontaDados->CampoData = $campoData;
            $objMontaDados->colunas = $camposTabelas;
            $objMontaDados->ArrayCamposOcutar = array(0 => "TELEFONE_PESSOA",1 => "CELULAR_PESSOA",2 => "DATA_CADASTRO_PESSOA");
            $objMontaDados->dados = $arrayDados;
            return $objMontaDados->deListar(2, "../../Controle/cad_Pessoa.php", 0, "&tipo=". $tipo ."");
        } else {
            return null;
        }
    }
 

    public function selecionar(Pessoa $obj) {

        $camposTabelas = array("P.ID_PESSOA","P.ID_GENERO","P.ID_ESTADO_CIVIL","P.NOME_PESSOA","P.CPF_PESSOA","P.RG_PESSOA","P.ORGAO_PESSOA","P.TELEFONE_PESSOA"
    ,"P.CELULAR_PESSOA","P.NATURALIDADE_PESSOA","P.DATA_CADASTRO_PESSOA","P.DATA_NASCIMENTO_PESSOA","P.STATUS_PESSOA","P.COMPL_END1_PESSOA","P.N_SUS_PESSOA","TIPO_PESSOA");
        $nomeTabelas = array("P"=>"PESSOA");
        $condicoes = array("P.ID_PESSOA = ". $obj->getIdPessoa() ." ");

        $dao = new DaoFull();
        $dao->table = $nomeTabelas;
        //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
        $d = $dao->selecionar($camposTabelas, $condicoes, null, null, null, null, null);
        if ($d != null) {
            $obj->setIdPessoa($d->dado[0]);
            $obj->setIdGenero($d->dado[1]);
            $obj->setIdEstadoCivil($d->dado[2]);
            $obj->setNomePessoa($d->dado[3]);
            $obj->setCpfPessoa($d->dado[4]);
            $obj->setRgPessoa($d->dado[5]);
            $obj->setOrgaoPessoa($d->dado[6]);
            $obj->setTelefonePessoa($d->dado[7]);
            $obj->setCelularPessoa($d->dado[8]);
            $obj->setNaturalidadePessoa($d->dado[9]);
            $obj->setDataCadastroPessoa($d->dado[10]);
            $obj->setDataNascimentoPessoa($d->dado[11]);
            $obj->setStatus($d->dado[12]);
            $obj->setComplementoEnd1Pessoa($d->dado[13]);
            $obj->setNsusPessoa($d->dado[14]);
            $obj->setTipo($d->dado[15]);
        } else {
            $obj->setIdPessoa(0);
        }
        return $obj;
    }

    public function PegarUltimoId() {
        $camposTabelas = array("P.ID_PESSOA");
        $nomeTabelas = array("P"=>"PESSOA");
        $condicoes = NULL;
        $dao = new DaoFull();
        $dao->table = $nomeTabelas;
        //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
        $d = $dao->selecionar($camposTabelas, $condicoes, "P.ID_PESSOA", "DESC", 1, null, null);
        if ($d != null) {
            $Id = $d->dado[0];
        } else {
            $Id = 0;
        }
        return $Id;
    }

    public function alterar(Pessoa $obj) {

        $dado = array($obj->getIdGenero(),$obj->getIdEstadoCivil(),$obj->getNomePessoa(),$obj->getCpfPessoa(),$obj->getRgPessoa(),$obj->getOrgaoPessoa(),$obj->getTelefonePessoa(),$obj->getCelularPessoa()
                ,$obj->getNaturalidadePessoa(),$obj->getDataNascimentoPessoa(),$obj->getComplementoEnd1Pessoa(),$obj->getNsusPessoa());//,$obj->getDataCadastroPessoa(),$obj->getStatus()
        $camposTabelas = array("ID_GENERO","ID_ESTADO_CIVIL","NOME_PESSOA","CPF_PESSOA","RG_PESSOA","ORGAO_PESSOA","TELEFONE_PESSOA","CELULAR_PESSOA",
    "NATURALIDADE_PESSOA","DATA_NASCIMENTO_PESSOA","COMPL_END1_PESSOA","N_SUS_PESSOA");//,"STATUS_PESSOA"
        $where = "ID_PESSOA =". $obj->getIdPessoa();

        $dao = new DaoFull();
        $dao->table = "PESSOA";

        if ($dao->Atualizar($dado, $camposTabelas, $where,null)) {
            return true;
        } else {
            return false;
        }
    }

    public function fucaoAtualizarDefull($dado, $camposTabelas, $where) {
        $dao = new DaoFull();
        $dao->table = "PESSOA";
        return $dao->Atualizar($dado, $camposTabelas, $where,null);
    }

    public function fucaoVerificarDefull($where) {
        $dao = new DaoFull();
        $dao->table = array("P" => "PESSOA");
        return $dao->Verificar($where,null);
    }

    public function excluir(Pessoa $obj) {

        $where = array("ID_PESSOA =". $obj->getIdPessoa());
        $dao = new DaoFull();
        $dao->table = "PESSOA";

        if ($dao->excluir($where,null)) {
            return true;
        } else {
            return false;
        }
    }

}
