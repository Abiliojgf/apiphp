<?php

/**
 * Description of DaoArrayGeneric
 *
 * @author AJ
 * CLASS DE ALIMENTA SELECT DE FORMULARILO
 */
class DaoArrayGeneric {

   private $Array;

   function __construct() {
      $this->Array = NULL;
   }

   //metodo de array de status Usuario
   public function ArrayStatus_Usuario() {
      $this->Array = NULL;
      $conn = new Conexao();
      $conn->sql = "select ID_STATUS as id,TIPO_STATUS as value from STATUS_USUARIO;";
      $ArrayPesquisa = $conn->montaArrayPesquisa();
      foreach ($ArrayPesquisa as $dados):
         $this->Array[] = array("id" => $dados["id"], "value" => $dados["value"]);
      endforeach;
      return $this->Array;
   }

   //metodo de array de Nivel de Acesso Usuario
   public function ArrayNivel_Usuario() {
      $this->Array = NULL;
      $conn = new Conexao();
      $conn->sql = "select ID_NIVEL as id,TIPO_NIVEL as value from NIVEL_USUARIO;";
      $ArrayPesquisa = $conn->montaArrayPesquisa();
      foreach ($ArrayPesquisa as $dados):
         $this->Array[] = array("id" => $dados["id"], "value" => $dados["value"]);
      endforeach;
      return $this->Array;
   }

   //metodo de array de Funções
   public function ArrayFuncoes() {
      $this->Array = NULL;
      $conn = new Conexao();
      $conn->sql = "select ID_FUNCOES as id,FUNCAO as value from FUNCOES;";
      $ArrayPesquisa = $conn->montaArrayPesquisa();
      foreach ($ArrayPesquisa as $dados):
         $this->Array[] = array("id" => $dados["id"], "value" => $dados["value"]);
      endforeach;
      return $this->Array;
   }

   //metodo de array de CARGO
   public function ArrayCargos() {
      $this->Array = NULL;
      $conn = new Conexao();
      $conn->sql = "select ID_CARGOS as id,TIPO_CARGO as value from CARGOS;";
      $ArrayPesquisa = $conn->montaArrayPesquisa();
      foreach ($ArrayPesquisa as $dados):
         $this->Array[] = array("id" => $dados["id"], "value" => $dados["value"]);
      endforeach;
      return $this->Array;
   }

   //metodo de array de Genero, ID_GENERO TIPO_GENERO
   public function ArrayGenero() {
      $this->Array = NULL;
      $conn = new Conexao();
      $conn->sql = "select ID_GENERO as id,TIPO_GENERO as value from GENERO;";
      $ArrayPesquisa = $conn->montaArrayPesquisa();
      foreach ($ArrayPesquisa as $dados):
         $this->Array[] = array("id" => $dados["id"], "value" => $dados["value"]);
      endforeach;
      return $this->Array;
   }

   //metodo de array de Estado Civil. ID_ESTADO_CIVIL TIPO_ESTADO_CIVIL
   public function ArrayEstadoCivil() {
      $this->Array = NULL;
      $conn = new Conexao();
      $conn->sql = "select ID_ESTADO_CIVIL as id,TIPO_ESTADO_CIVIL as value from ESTADO_CIVIL;";
      $ArrayPesquisa = $conn->montaArrayPesquisa();
      foreach ($ArrayPesquisa as $dados):
         $this->Array[] = array("id" => $dados["id"], "value" => $dados["value"]);
      endforeach;
      return $this->Array;
   }

   //metodo de array de Tipo Pessoa. ID_TIPO_PESSOA TIPO_PESSOA
   public function ArrayTipoPessoa() {
      $this->Array = NULL;
      $conn = new Conexao();
      $conn->sql = "select ID_TIPO_PESSOA as id,TIPO_PESSOA as value from TIPO_PESSOA;";
      $ArrayPesquisa = $conn->montaArrayPesquisa();
      foreach ($ArrayPesquisa as $dados):
         $this->Array[] = array("id" => $dados["id"], "value" => $dados["value"]);
      endforeach;
      return $this->Array;
   }

}
