<?php

/**
 * Description of DaoEnderecoPessoa
 *
 * @author aj
 */
class DaoEnderecoPessoa {

   // ENDERECO_PESSOA "ID","ID_PESSOA"
   public function inserir(EnderecoPessoa $obj) {
      $dado = array($obj->getID(), $obj->getIDPESSOA());
      $coluna = null; //array("ID","ID_PESSOA");
      $dao = new DaoFull();
      $dao->table = "ENDERECO_PESSOA";
      return $dao->inserir($dado, $coluna, null);
   }

   public function Listar() {
   }

   public function selecionar(EnderecoPessoa $obj) {
      $camposTabelas = array("EP.ID", "EP.ID_PESSOA");
      $nomeTabelas = array("EP" => "ENDERECO_PESSOA");
      $condicoes = array("EP.ID_PESSOA=" . $obj->getIDPESSOA() . "");
      $dao = new DaoFull();
      $dao->arrayTable = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
      $d = $dao->selecionar($camposTabelas, $condicoes, null, null, null, null, null);
      $ObjReturn = new EnderecoPessoa();
      if ($d != null) {
         $ObjReturn->setID($d->dado[0]);
         $ObjReturn->setIDPESSOA($d->dado[1]);
      } else {
         $ObjReturn->setID(0);
      }
      return $ObjReturn;
   }

   public function selecionarPorID_PESSOA(EnderecoPessoa $obj) {
      $camposTabelas = array("EP.ID", "EP.ID_PESSOA");
      $nomeTabelas = array("EP" => "ENDERECO_PESSOA");
      $condicoes = array("EP.ID_PESSOA=" . $obj->getIDPESSOA() . "");
      $dao = new DaoFull();
      $dao->table = $nomeTabelas;
      //$camposTabelas, $condicoes, $colunaOrdenada, $ordenacao, $limit, $TOP,$ArrayTo
      $d = $dao->selecionar($camposTabelas, $condicoes, null, null, null, null, null);
      $ObjReturn = new EnderecoPessoa();
      if ($d != null) {
         $ObjReturn->setID($d->dado[0]);
         $ObjReturn->setIDPESSOA($d->dado[1]);
      } else {
         $ObjReturn->setID(0);
         $ObjReturn->setIDPESSOA(0);
      }
      return $ObjReturn;
   }

   public function alterar(EnderecoPessoa $obj) {
      $dado = array($obj->getID()); //,$obj->getIDPESSOA()
      $camposTabelas = array("ID"); //,"ID_PESSOA"
      $where = "ID_PESSOA=" . $obj->getIDPESSOA() . "";
      $dao = new DaoFull();
      $dao->table = "ENDERECO_PESSOA";
      if ($dao->Atualizar($dado, $camposTabelas, $where, null)) {
         return true;
      } else {
         return false;
      }
   }

   public function fucaoAtualizarDefull($dado, $camposTabelas, $where) {
      $dao = new DaoFull();
      $dao->table = "ENDERECO_PESSOA";
      return $dao->Atualizar($dado, $camposTabelas, $where, null);
   }

   public function fucaoVerificarDefull($where) {
      $dao = new DaoFull();
      $dao->table = array("EP" => "ENDERECO_PESSOA");
      return $dao->Verificar($where, null);
   }

   public function excluir(EnderecoPessoa $obj) {
      $where = array("ID_PESSOA=" . $obj->getIDPESSOA() . " ");
      $dao = new DaoFull();
      $dao->table = "ENDERECO_PESSOA";
      if ($dao->excluir($where, null)) {
         return true;
      } else {
         return false;
      }
   }

}
