<?php
//config e class padrao
 require_once("../config.php");
// lstUsuario.php frmUsuario.php       controle/cad_Usuario.php

// Importar classes PHPMailer para o namespace global
// Estes devem estar no topo do seu script, não dentro de uma função

//require '../vendor/phpmailer/phpmailer/src/Exception.php';
//require '../vendor/phpmailer/phpmailer/src/PHPMailer.php';

//use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;

//class
$data = new Data();
$objUsuario = new Usuario();
$Dao = new DaoUsuario();

$acao = Request::Do_REQUEST("acao", "");

switch ($acao):
   case "n":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmUsuario.php?id=" . $id . "&op=0");
      break;
   case "s":
      $id = Request::Do_POST("id", 0);
      $objUsuario->setId($id);
      $objUsuario->setNome(ToString::setToAspaSAspaD(Request::Do_POST("nome", "")));
      $objUsuario->setLogin(ToString::setToAspaSAspaD(Request::Do_POST("login", "")));
      $objUsuario->setEmail(ToString::setToAspaSAspaD(Request::Do_POST("email", "")));
      $objUsuario->setTelefone(ToString::setToAspaSAspaD(Request::Do_POST("telefone", "")));
      $objUsuario->setTipo(1);
      $objUsuario->setStatus(Request::Do_POST("status", 3));
      $objUsuario->setNivel(Request::Do_POST("nivel", 2));
      $objUsuario->setDatacadastro($data->data_atual_en());
      $objUsuario->setDataalteracao($data->data_atual_en());
      $objUsuario->setDataultimologin($data->dataEhora_atual_en());
      $cargo = Request::Do_POST("cargo", 0);
      $dao = new DaoUsuario();
      $daoUI = new DaoUsuarioInterno();
      if ($objUsuario->getId() == 0) {
         $objUsuario->setSenha(ToString::criptografaMD5(Request::Do_POST("senha", '')));
         if ($dao->inserir($objUsuario)) {
            $ID_USUARIO = $dao->PegarUltimoId();
            $objUI = new UsuarioInterno($cargo, $ID_USUARIO);
            if ($daoUI->inserir($objUI)):
               header("location:../admin/lst/lstUsuario.php?msg=1");
            else:
               header("location:../admin/lst/lstUsuario.php?msg=1");
            endif;
         } else {
            header("location:../admin/lst/lstUsuario.php?msg=2");
         }
      } else {
         if ($dao->alterar($objUsuario)) {
            $ID_USUARIO = $objUsuario->getId();
            $objUI = new UsuarioInterno($cargo, $ID_USUARIO);
            if ($daoUI->alterar($objUI)):
               header("location:../admin/lst/lstUsuario.php?msg=1");
            else:
               header("location:../admin/lst/lstUsuario.php?msg=1");
            endif;
         } else {
            header("location:../admin/lst/lstUsuario.php?msg=2");
         }
      }
      break;
   case "a":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmUsuario.php?id=" . $id . "&op=1");
      break;
   case "v":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmUsuario.php?id=" . $id . "&op=2");
      break;
   case "ai":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=0");
      break;
   case "sc":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=1");
      break;
   case "as":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=2");
      break;
   case "alteraSenha":
      $id = Request::Do_REQUEST("id", 0);
      $objUsuario = new Usuario();
      $objUsuario->setId($id);
      $senha = ToString::criptografaMD5(Request::Do_POST("senha", ''));
      $senhanova = Request::Do_POST("senhan", '');
      $senhanovaN = Request::Do_POST("senhanN", '');
      $dao = new DaoUsuario();
      $usu = $dao->selecionar($objUsuario);
      if ($senha == $usu->getSenha()):
         if ($senhanova == $senhanovaN):
            $senhanovaI = ToString::criptografaMD5($senhanova);
            if ($dao->fucaoAtualizarDefull(array($senhanovaI), array("SENHA_USUARIO"), "ID_USUARIO={$id}")):
               header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=1&msg=1");
            else:
               header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=1&msg=2");
            endif;
         else:
            header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=2&msg=3");
         endif;
      else:
         header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=2&msg=4");
      endif;
      break;
   case "up":
      $id = Request::Do_POST("id", 0);
      $Dao = new DaoUsuario();
      $caminho = "../admin/assets/images/user/"; // Este é o caminho onde as imagens serão guardadas no servidor
      // Verificámos se o upload foi efectuado para o directório / caminho definido (juntámos também o nome da imagem)
      $eXT = strtolower(strrchr($_FILES['imagemupload']['name'], "."));
      $imagem = "user" . $id . $eXT;
      if (move_uploaded_file($_FILES['imagemupload']['tmp_name'], $caminho . $imagem)) {
         // Utilizámos agora a "magia" da Classe para criar os tamanhos necessários
         $resize_tamanho1 = new resize($caminho . $imagem);
         $resize_tamanho2 = new resize($caminho . $imagem);
         // Definimos as medidas que cada tamanho irá ter
         $resize_tamanho1->resizeImage(120, 120, 'crop');
         $resize_tamanho2->resizeImage(338, 338, 'crop');
         // Renomeámos a imagem para que seja possivel o mesmo nome ter vários tamanhos
         $tamanho1 = "t1_" . $imagem;
         $tamanho2 = "t2_" . $imagem;
         // Para finalizar guardámos a im0agem. Definimos o caminho, qual o nome e a qualidade
         if ($resize_tamanho1->saveImage($caminho . $tamanho1, 100) && $resize_tamanho2->saveImage($caminho . $tamanho2, 100)) {
            // O próximo passo é opcional mas recomendável.
            // Apagámos a imagem original
            unlink($caminho . $imagem);
            if ($Dao->fucaoAtualizarDefull(array($imagem), array("FOTO_USUARIO"), "ID_USUARIO={$id}")) {
               header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=0&msg=1");
            } else {
               header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=0&msg=2");
            }
         } else {
            header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=0&msg=2");
         }
      } else {
         // No caso de erro no upload, podem redireccionar para outra página ou criar uma notificação
         header("location:../admin/frm/frmUsuarioSC.php?id=" . $id . "&op=0&msg=2");
      }
      break;
   case "l":
      header("location:../admin/lst/lstUsuario.php");
      break;
   case "d":
      $id = Request::Do_GET("id", 0);
      $un = new Usuario();
      $un->setId($id);
      $dao = new DaoUsuario();
      $u = $dao->selecionar($un);
      if ($u->getStatus() == 3) {
         $un->setStatus(2);
      } elseif ($u->getStatus() == 2) {
         $un->setStatus(3);
      }
      if ($dao->fucaoAtualizarDefull(array($un->getStatus()), array("ID_STATUS"), "ID_USUARIO={$un->getId()}")) {
         header("location:../admin/lst/lstUsuario.php?msg=1");
      } else {
         header("location:../admin/lst/lstUsuario.php?msg=2");
      }
      break;
   case "e":
      $id = Request::Do_GET("id", 0);
      $objUsuario = new Usuario();
      $objUsuario->setId($id);
      $dao = new DaoUsuario();
      if ($dao->excluir($objUsuario)) {
         header("location:../admin/lst/lstUsuario.php?msg=1");
      } else {
         header("location:../admin/lst/lstUsuario.php?msg=2");
      }
      break;
   case "RS":
      $emaili = Request::Do_GET("email", "");
      $nome = "";
      $login = "";
      $senhanova = ToString::makeRandomPassword();
      $dao = new DaoUsuario();
      $IsOK = $dao->fucaoVerificarDefull(array("U.EMAIL_USUARIO = '" . $emaili . "'"));
      if (true)://$IsOK
//         $conn = new Conexao();
//         $conn->sql = "select ID_USUARIO,NOME_USUARIO,LOGIN_USUARIO,EMAIL_USUARIO from USUARIO where EMAIL_USUARIO = '{$emaili}';";
//         $result = $conn->executaQuery();
//         while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
//            $id = $row['ID_USUARIO'];
//            $nome = $row['NOME_USUARIO'];
            $email = 'ajgfqbu@gmail.com';//$row['EMAIL_USUARIO'];
//            $login = $row['LOGIN_USUARIO'];
//         }

         $message = "Olá {$nome}, redefinimos sua senha.<br /><br />
      <strong>Seu login é</strong>: {$login}<br /><br />
      <strong>Sua Nova Senha é</strong>: {$senhanova}<br /><br />
      <a href=''>Imoveis</a><br /><br />
      Obrigado!<br /><br />
      <br /><br /><br />
      Esta é uma mensagem automática, por favor não responda!";

         $phpmailerUtil = new PhpmailerUtil();
         $phpmailerUtil->I_Remetente_Destinatários($email, $nome, 1, null, null);
         $phpmailerUtil->IIConfigMsg(true);
         $phpmailerUtil->IIIDefineMensagem("Sua nova senha em App CASL", $message);
         $senhanovaI = ToString::criptografaMD5($senhanova);
         $conn->sql = "UPDATE USUARIO SET SENHA_USUARIO='{$senhanovaI}' WHERE ID_USUARIO='{$id}'";
         if (true)://$conn->executaQuery()
            if ($phpmailerUtil->IVEnviar()) :
               header('location: ../admin/login.php?msg=6');
            else:
               header('location: ../admin/login.php?msg=7');
            endif;
         else:
            header('location: ../admin/login.php?msg=7');
         endif;
      else:
         header('location: ../admin/login.php?msg=8');
      endif;
      break;
   case "logar":
      $Service->Error_ReportingLogin();
      //abre o buffer para inicializar os cookies
      ob_start();
      //inicializa a sessao
      session_start();
      $String = new ToString();
      $login = Request::Do_POST("login", null);
      $senha = Request::Do_POST("senha", null);
      $dao = new DaoUsuario();
      $usu = new Usuario();
      $usuv = new Usuario();
      $usu->setLogin($login);
//        $usu->setSenha($senha);
//        $usuv->setSenha($senha);
      $usu->setSenha(ToString::criptografaMD5($senha));
      $usuv->setSenha(ToString::criptografaMD5($senha));
      $usuL = $dao->logar($usu);
      if ($usuL->getLogin() == $login) {//verifica login
         if ($usuL->getSenha() == $usuv->getSenha()) {//verifica senha
            if ($usuL->getNivel() != 1 || $usuL->getNivel() != 2) {
               if ($usuL->getStatus() != 1) {
                  if ($usuL->getStatus() != 2) {
                     $_SESSION['idusu' . $Service->getNameSESSION() . ''] = $usuL->getId();
                     header('location: ../admin/index.php');
                  } else {
                     header('location: ../admin/login.php?msg=4');
                  }
               } else {
                  header('location: ../admin/login.php?msg=7');
               }
            } else {
               header('location: ../admin/login.php?msg=3');
            }
         } else {
            header('location: ../admin/login.php?msg=2');
         }
      } else {
         header('location: ../admin/login.php?msg=1');
      }
      break;
   case "sair":
      session_start();
      if (isset($_SESSION['idusu' . $Service->getNameSESSION() . ''])) {
         unset($_SESSION['idusu' . $Service->getNameSESSION() . '']);
      }
      header('location: ../admin/login.php?msg=5'); //../admin/login.php?msg=5
      break;
   default :
//      header('location: ../admin/index.php?msg=503'); //../admin/login.php?msg=5
      break;
endswitch;
