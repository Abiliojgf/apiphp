<?php
//config e class padrao
require_once("../config.php");

// lstCargo.php frmCargo.php       controle/cad_Cargo.php

//class
$objC = new Cargo();
$daoC = new DaoCargo();

//Variaveis
$acao = Request::Do_REQUEST("acao", "");

switch ($acao):
   case "n":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmCargo.php?id=" . $id . "&op=0");
      break;
   case "s":
      $id = Request::Do_POST("id", 0);
      $objC->setIDCARGOS($id);
      $objC->setTIPOCARGO(Request::Do_POST("nome", ""));

      if ($objC->getIDCARGOS() == 0):
         if ($daoC->inserir($objC)) {
            header("location:../admin/lst/lstCargo.php?msg=1");
         } else {
            header("location:../admin/lst/lstCargo.php?msg=2");
         }
      else:
         if ($daoC->alterar($objC)) {
            header("location:../admin/lst/lstCargo.php?msg=1");
         } else {
            header("location:../admin/lst/lstCargo.php?msg=2");
         }
      endif;
      break;
   case "a":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmCargo.php?id=" . $id . "&op=1");
      break;
   case "v":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmCargo.php?id=" . $id . "&op=2");
      break;
   case "l":
      header("location:../admin/lst/lstCargo.php");
      break;
   case "d":
      $id = Request::Do_GET("id", 0);
      $objC = new Cargo();
      $objC->setIDCARGOS($id);
      if (TRUE) {
         header("location:../admin/lst/lstCargo.php?msg=1");
      } else {
         header("location:../admin/lst/lstCargo.php?msg=2");
      }
      break;
   case "e":
      $id = Request::Do_GET("id", 0);
      $objC = new Cargo();
      $objC->setIDCARGOS($id);
      if ($daoC->excluir($objC)) {
         header("location:../admin/lst/lstCargo.php?msg=1");
      } else {
         header("location:../admin/lst/lstCargo.php?msg=2");
      }
      break;
   default :
      session_destroy();
      header('location: ../index.php'); //../admin/login.php?msg=5
      break;
endswitch;
