<?php

//config e class padrao
require_once("../config.php");

// lstPessoa.php frmPessoa.php       controle/cad_Pessoa.php

//class Padrão api
$Data = new Data();

$String = new ToString(null);
$dao = new DaoPessoa();
$DaoE = new DaoEndereco();
$DaoEP = new DaoEnderecoPessoa();
$objP = new Pessoa();
$objE = new Endereco();
$objEP = new EnderecoPessoa();
$resuDefat = "";

$msg = 0;
$acao = Request::Do_GET("acao", "");
$tipo = Request::Do_GET("tipo", 0);

switch ($acao):
   case "n":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmPessoa.php?id=" . $id . "&op=0&tipo=" . $tipo . "");
      break;
   case "s":
      $id = Request::Do_POST("id", 0);
      $resuDefat = "";
      $IsOK = false;
      $idPessoa = 0;

      $objP->setIdPessoa((int) Request::Do_POST("id", 0));
      $objP->setNomePessoa(Request::Do_POST("nome", ""));
      $objP->setRgPessoa(Request::Do_POST("rg", ""));
      $objP->setCpfPessoa(Request::Do_POST("cpf", ""));
      $objP->setOrgaoPessoa(Request::Do_POST("orgao", ""));
      $objP->setIdGenero(Request::Do_POST("genero", 0));
      $objP->setIdEstadoCivil(Request::Do_POST("estadocivil", 0));
      $objP->setTelefonePessoa(Request::Do_POST("telefone", ""));
      $objP->setCelularPessoa(Request::Do_POST("celular", ""));
      $objP->setNaturalidadePessoa(Request::Do_POST("naturalidade", ""));
      $objP->setComplementoEnd1Pessoa(Request::Do_POST("complementoend1", ""));
      $objP->setNsusPessoa(Request::Do_POST("nsus", ""));
      $objP->setTipo($tipo);

      //cep logradouro bairro cidade uf
      $objE->setId(0);
//        $objE->setCep(Request::Do_POST("cep", null));
      $objE->setCep($String->TrocarCaracte("-", "", Request::Do_POST("cep", null)));
      $objE->setLogradouro(Request::Do_POST("logradouro", $resuDefat));
      $objE->setBairro(Request::Do_POST("bairro", $resuDefat));
      $objE->setCidade(Request::Do_POST("cidade", $resuDefat));
      $objE->setUf(Request::Do_POST("uf", $resuDefat));
      $objE->setFlag((int) Request::Do_POST("flag", 1));

      // PRA DATA EM RELAÇÃO Browser
      $info = new GetInfoSettings();
      $info->GETNavegador();

      if ($info->getBrowserID() == 2):
         $objP->setDataNascimentoPessoa($Data->data_user_para_mysql(Request::Do_POST("datanascimento", $Data->getDataDefatPT_BR())));
      else:
         $objP->setDataNascimentoPessoa(Request::Do_POST("datanascimento", $Data->getDataDefat()));
      endif;

      //$DaoEP->fucaoAtualizarDefull(array(2), array("ID"), "ID_PESSOA=2");

      if ($objP->getIdPessoa() == 0) {
         $objP->setDataCadastroPessoa($Data->data_atual_en());
         $objP->setStatus((int) 1);
         if ($dao->inserir($objP)) {
            $msg = 1;

            $idPessoa = (int) $dao->PegarUltimoId();
            $idenderecoI = 0;

            if ($objE->getCep() != ""):
               $idenderecoI = $DaoE->PegarIdpeloCEP($objE->getCep()); //pega id pelo cep
               if ($idenderecoI == 0): //valida enderenços
                  $IsOK = $DaoE->inserir($objE);
                  $idenderecoI = $DaoE->PegarUltimoId();
               else:
                  $objE->setId($idenderecoI);
                  $IsOK = $DaoE->alterar($objE);
               endif;
            else:
               if ($objE->getLogradouro() != "" || $objE->getBairro() != "" || $objE->getCidade() != "" || $objE->getUf() != ""):
                  $IsOK = $DaoE->inserir($objE);
                  $idenderecoI = $DaoE->PegarUltimoId();
               endif;
            endif;

            if ($idenderecoI != 0):
               $objEP = new EnderecoPessoa();
               $objEP->setID((int) $idenderecoI);
               $objEP->setIDPESSOA((int) $idPessoa);
               $idEnderecoEP = $DaoEP->selecionarPorID_PESSOA($objEP); //buscar se a regito desse Pessoa posu End

               if ($IsOK == true):
                  $msg = 1;
                  if ($idEnderecoEP->getIDPESSOA() == 0) :
                     if (!$DaoEP->inserir($objEP)):
                        $msg = 3;
                     endif;
                  endif;
               endif;
            endif;

            header("location:../admin/lst/lstPessoa.php?msg=" . $msg . "&tipo=" . $tipo . "");
         } else {
            header("location:../admin/lst/lstPessoa.php?msg=2&tipo=" . $tipo . "");
         }
      } else {
         if ($dao->alterar($objP)) {
            $msg = 1;
            $idPessoa = $objP->getIdPessoa();
            $idenderecoI = 0;
            $idenderecoII = 0;
            $idenderecoIII = 0;

            //SE IDIDPESSOA ta viculado A PESSOA
            $objEPNn = new EnderecoPessoa();
            $objEPNn->setIDPESSOA($idPessoa);
            $objEPN = $DaoEP->selecionarPorID_PESSOA($objEPNn);
            $idenderecoI = $objEPN->getID();

            $idenderecoIII = (int) $idenderecoI;

            //Verificar o id de endereço  do BD anterio
            $objE_NV = new Endereco();
            $objE_NV->setId($idenderecoI);
            $OBJ_V = $DaoE->selecionar_Verificar($objE);

            $idenderecoII = ($objE->getCep() != null) ? (int) $DaoE->PegarIdpeloCEP($objE->getCep()) : 0; //pega id pelo cep
            $idenderecoI = (int) ($idenderecoII >= 1) ? ($idenderecoII != $idenderecoI) ? (int) $idenderecoII : $idenderecoI : $idenderecoI;

//                if ($OBJ_V) $idenderecoI = $idenderecoIII;

            if ($objE->getCep() != null):
               if ($idenderecoI == 0): //valida enderenços>=
                  $IsOK = $DaoE->inserir($objE);
                  $idenderecoI = $DaoE->PegarUltimoId();
               else:
                  $objE->setId($idenderecoI);
                  $IsOK = $DaoE->alterar($objE);
               endif;
            else:
               if ($idenderecoI == 0):
                  if ($objE->getCep() != "" || $objE->getLogradouro() != "" || $objE->getBairro() != "" || $objE->getCidade() != "" || $objE->getUf() != ""):
                     $IsOK = $DaoE->inserir($objE);
                     $idenderecoI = $DaoE->PegarUltimoId();
                  endif;
               else:
                  $objE->setId($idenderecoI);
                  $IsOK = $DaoE->alterar($objE);
               endif;
            endif;

            if ($idenderecoI != 0):
               $objEP = new EnderecoPessoa();
               $objEP->setID((int) $idenderecoI);
               $objEP->setIDPESSOA((int) $idPessoa);
               $idEnderecoEP = $DaoEP->selecionar($objEP); //buscar se a regito desse Pessoa posu End

               if ($IsOK == true):
                  $msg = 1;
                  if ($idEnderecoEP->getID() == 0):
                     if (!$DaoEP->inserir($objEP)):
                        $msg = 3;
                     endif;
                  else:
                     if ($idEnderecoEP->getID() != $idenderecoI):
                        if (!$DaoEP->alterar($objEP)):
                           $msg = 3;
                        endif;
                     endif;
                  endif;
               endif;
            endif;

            header("location:../admin/lst/lstPessoa.php?msg=" . $msg . "&tipo=" . $tipo . "");
         } else {
            header("location:../admin/lst/lstPessoa.php?msg=2&tipo=" . $tipo . "");
         }
      }
      break;
   case "a":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmPessoa.php?id=" . $id . "&op=1&tipo=" . $tipo . "");
      break;
   case "v":
      $id = Request::Do_GET("id", 0);
      header("location:../admin/frm/frmPessoa.php?id=" . $id . "&op=2&tipo=" . $tipo . "");
      break;
   case "l":
      header("location:../admin/lst/lstPessoa.php?tipo=" . $tipo . "");
      break;
   case "d":
      $id = Request::Do_GET("id", 0);
      $Vn = new Pessoa;
      $Vn->setIdPessoa($id);
      $dao = new DaoPessoa();
      $V = $dao->selecionar($Vn);
      if ($V->getStatus() == 1) {
         $Vn->setStatus(0);
      } elseif ($V->getStatus() == 0) {
         $Vn->setStatus(1);
      }
      if ($dao->fucaoAtualizarDefull(array($Vn->getStatus()), array("STATUS_PESSOA"), "ID_PESSOA={$Vn->getIdPessoa()}")) {
         header("location:../admin/lst/lstPessoa.php?msg=1&tipo=" . $tipo . "");
      } else {
         header("location:../admin/lst/lstPessoa.php?msg=2&tipo=" . $tipo . "");
      }
      break;
   case "e":
      $id = Request::Do_GET("id", 0);
      $objP->setIdPessoa($id);
      $dao = new DaoPessoa();
      if ($dao->excluir($objP)) {
         header("location:../admin/lst/lstPessoa.php?msg=1&tipo=" . $tipo . "");
      } else {
         header("location:../admin/lst/lstPessoa.php?msg=2&tipo=" . $tipo . "");
      }
      break;
   default :
//        session_destroy();
//        header('location: ../index.php'); //../admin/login.php?msg=5
      break;
endswitch;
