#########################################################################################################
##                                          APAGAR BANCO DE DADOS                                      ##
#########################################################################################################
DROP DATABASE apiphp;

#########################################################################################################
##                                           CRIAR BANCO DE DADOS                                      ##
#########################################################################################################
CREATE DATABASE apiphp;

ALTER DATABASE apiphp DEFAULT CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI ;

USE apiphp;

#########################################################################################################
##                                           CRIAR TABELAS                                             ##
#########################################################################################################
	
/*==============================================================*/
/* TABLE: CONTATO                                                */
/*==============================================================*/

CREATE TABLE IF NOT EXISTS CONTATO (
  ID_CONTATO INT(11) NOT NULL AUTO_INCREMENT,
  NOME_CONTATO VARCHAR(50)  DEFAULT NULL,
  EMAIL_CONTATO VARCHAR(50)  DEFAULT NULL,
  TELEFONE_CONTATO VARCHAR(20)  DEFAULT NULL,
  ASSUNTO_CONTATO VARCHAR(50)  DEFAULT NULL,
  MENSSAGEM_CONTATO VARCHAR(450),
  DATAENVIADA_CONTATO DATE DEFAULT NULL,
  LIDA_CONTATO INT(11) DEFAULT 0,
  PRIMARY KEY (ID_CONTATO)
) ENGINE=INNODB DEFAULT CHARSET=UTF8;

	
/*==============================================================*/
/* TABLE: CARGOS                                                */
/*==============================================================*/

CREATE TABLE CARGOS
(
   ID_CARGOS            INTEGER(11) NOT NULL AUTO_INCREMENT,
   TIPO_CARGO           VARCHAR(50) NOT NULL,
   PRIMARY KEY (ID_CARGOS)
);

/*==============================================================*/
/* TABLE: CONTATO_EMAIL                                         */
/*==============================================================*/
CREATE TABLE CONTATO_EMAIL
(
   ID_CONTATO_EMAIL     INTEGER(11) NOT NULL AUTO_INCREMENT,
   CONTATO_EMAIL        VARCHAR(100) NOT NULL,
   PRIMARY KEY (ID_CONTATO_EMAIL)
);

/*==============================================================*/
/* TABLE: CONTATO_TEL                                           */
/*==============================================================*/
CREATE TABLE CONTATO_TEL
(
   ID_CONTATO_TEL       INTEGER(11) NOT NULL AUTO_INCREMENT,
   NUMERO_CONTATO_TEL   VARCHAR(100) NOT NULL,
   TIPO_CONTATO_TEL     INTEGER(2),
   PRIMARY KEY (ID_CONTATO_TEL)
);

/*==============================================================*/
/* TABLE: NIVEL_USUARIO                                         */
/*==============================================================*/
CREATE TABLE NIVEL_USUARIO
(
   ID_NIVEL             INTEGER(11) NOT NULL AUTO_INCREMENT,
   TIPO_NIVEL           VARCHAR(20) NOT NULL,
   PRIMARY KEY (ID_NIVEL)
);

/*==============================================================*/
/* TABLE: STATUS_USUARIO                                        */
/*==============================================================*/
CREATE TABLE STATUS_USUARIO
(
   ID_STATUS            INTEGER(11) NOT NULL AUTO_INCREMENT,
   TIPO_STATUS          VARCHAR(20) NOT NULL,
   PRIMARY KEY (ID_STATUS)
); 
 
/*==============================================================*/
/* TABLE: USUARIO                                               */
/*==============================================================*/
CREATE TABLE USUARIO
(
   ID_USUARIO           INTEGER(11) NOT NULL AUTO_INCREMENT, 
   ID_STATUS            INTEGER(11),
   ID_NIVEL             INTEGER(11),
   NOME_USUARIO         VARCHAR(100) NOT NULL,
   LOGIN_USUARIO        VARCHAR(20),
   SENHA_USUARIO        VARCHAR(64),
   EMAIL_USUARIO        VARCHAR(250),
   TELEFONE_USUARIO     VARCHAR(10), 
   DATA_CADASTRO_USUARIO DATE,
   DATA_ALTERACAO_USUARIO DATE,
   DATA_ULTIMO_LOGIN_USUARIO DATETIME,
   FOTO_USUARIO         VARCHAR(250),
   PRIMARY KEY (ID_USUARIO)
);

/*==============================================================*/
/* TABLE: USUARIO_INTERNO                                       */
/*==============================================================*/
CREATE TABLE USUARIO_INTERNO
(
   ID_CARGOS            INT,
   ID_USUARIO           INT
);

/*==============================================================*/
/* TABLE: PESSOA_TELEFONE                                      */
/*==============================================================*/
CREATE TABLE PESSOA_TELEFONE
(
   ID_CONTATO_TEL       INT,
   ID_PESSOA           INT
);

/*==============================================================*/
/* TABLE: PESSOA_EMAIL                                         */
/*==============================================================*/
CREATE TABLE PESSOA_EMAIL
(
   ID_PESSOA           INT,
   ID_CONTATO_EMAIL     INT
);

/*==============================================================*/
/* TABLE: ENDERECO                                              */
/*==============================================================*/
CREATE TABLE ENDERECO
(
   ID                   INTEGER NOT NULL AUTO_INCREMENT,
   CIDADE               VARCHAR(50),
   LOGRADOURO           VARCHAR(70),
   BAIRRO               VARCHAR(72),
   CEP                  VARCHAR(9),
   TP_LOGRADOURO        VARCHAR(20),
   UF                   CHAR(2),
   FLAG                 INTEGER,
   PRIMARY KEY (ID)
);

/*==============================================================*/
/* TABLE: ENDERECO_PESSOA                                     */
/*==============================================================*/
CREATE TABLE ENDERECO_PESSOA
(
   ID                 INTEGER,
   ID_PESSOA          INTEGER
);

/*==============================================================*/
/* TABLE: ESTADO_CIVIL                                          */
/*==============================================================*/
CREATE TABLE ESTADO_CIVIL
(
   ID_ESTADO_CIVIL      INTEGER NOT NULL AUTO_INCREMENT,
   TIPO_ESTADO_CIVIL    VARCHAR(20) NOT NULL,
   PRIMARY KEY (ID_ESTADO_CIVIL)
);

/*==============================================================*/
/* TABLE: GENERO                                                */
/*==============================================================*/
CREATE TABLE GENERO
(
   ID_GENERO            INTEGER NOT NULL AUTO_INCREMENT,
   TIPO_GENERO          VARCHAR(20) NOT NULL,
   PRIMARY KEY (ID_GENERO)
);

/*==============================================================*/
/* TABLE: TIPO_PESSOA                                          */
/*==============================================================*/
CREATE TABLE TIPO_PESSOA
(
   ID_TIPO_PESSOA      INTEGER NOT NULL AUTO_INCREMENT,
   TIPO_PESSOA    VARCHAR(20) NOT NULL,
   PRIMARY KEY (ID_TIPO_PESSOA)
);

/*==============================================================*/
/* TABLE: PESSOA                                              */
/*==============================================================*/
CREATE TABLE PESSOA
(
   ID_PESSOA          INTEGER NOT NULL AUTO_INCREMENT,
   ID_GENERO          INTEGER,
   ID_ESTADO_CIVIL    INTEGER,
   NOME_PESSOA        VARCHAR(200) NOT NULL,
   CPF_PESSOA         VARCHAR(11),
   RG_PESSOA          VARCHAR(20),
   ORGAO_PESSOA       VARCHAR(10),
   TELEFONE_PESSOA    VARCHAR(11),
   CELULAR_PESSOA     VARCHAR(11),
   NATURALIDADE_PESSOA VARCHAR(100),
   DATA_CADASTRO_PESSOA DATE,
   DATA_NASCIMENTO_PESSOA DATE,
   STATUS_PESSOA      CHAR,
   COMPL_END1_PESSOA  VARCHAR(50),
   TIPO_PESSOA        INT,
   PRIMARY KEY (ID_PESSOA)
);

CREATE TABLE DADOS_PESSOAIS_USUARIO
(
   ID_PESSOA      INT,
   ID_USUARIO     INT
);

#########################################################################################################
##                                           Inserts                                                   ##
#########################################################################################################

-- Emails
-- INSERT INTO CONTATO_EMAIL (ID_CONTATO_EMAIL,CONTATO_EMAIL) VALUES (NULL,'admin@FULL.com.br');

-- Telefones
-- INSERT INTO CONTATO_TEL (ID_CONTATO_TEL,NUMERO_CONTATO_TEL,TIPO_CONTATO_TEL) VALUES (NULL,'6233111836',1);    

-- nivel
INSERT INTO NIVEL_USUARIO (ID_NIVEL,TIPO_NIVEL) VALUES (1,'Sem permição');
INSERT INTO NIVEL_USUARIO (ID_NIVEL,TIPO_NIVEL) VALUES (2,'Sem Acesso');
INSERT INTO NIVEL_USUARIO (ID_NIVEL,TIPO_NIVEL) VALUES (3,'Usuário');
INSERT INTO NIVEL_USUARIO (ID_NIVEL,TIPO_NIVEL) VALUES (4,'Administrador');
INSERT INTO NIVEL_USUARIO (ID_NIVEL,TIPO_NIVEL) VALUES (5,'Super Usuário');

-- status
INSERT INTO STATUS_USUARIO (ID_STATUS,TIPO_STATUS) VALUES (NULL,'Cancelado');
INSERT INTO STATUS_USUARIO (ID_STATUS,TIPO_STATUS) VALUES (NULL,'Inativo');
INSERT INTO STATUS_USUARIO (ID_STATUS,TIPO_STATUS) VALUES (NULL,'Ativo');

-- cargo
INSERT INTO CARGOS (ID_CARGOS,TIPO_CARGO) VALUES (NULL,'Administrador');
INSERT INTO CARGOS (ID_CARGOS,TIPO_CARGO) VALUES (NULL,'Usuário');
INSERT INTO CARGOS (ID_CARGOS,TIPO_CARGO) VALUES (NULL,'Atendente');

/*Usuarios*/ 
INSERT INTO usuario (ID_USUARIO, ID_STATUS, ID_NIVEL, NOME_USUARIO, LOGIN_USUARIO, SENHA_USUARIO, EMAIL_USUARIO, TELEFONE_USUARIO, DATA_CADASTRO_USUARIO, DATA_ALTERACAO_USUARIO, DATA_ULTIMO_LOGIN_USUARIO, FOTO_USUARIO) 
VALUES (1, 3, 5, 'Admin', 'admin', md5('admin'), 'admin@ADMIN.com.br', '', CURRENT_DATE(), CURRENT_DATE(), CURRENT_DATE(), null);

/*Usuario Interno*/
INSERT INTO USUARIO_INTERNO (ID_CARGOS, ID_USUARIO) 
VALUES (1, 1);

 # Inserir os cadastros bases

    # ESTADO CIVIL 
    INSERT INTO ESTADO_CIVIL (TIPO_ESTADO_CIVIL) VALUES ('SOLTEIRO');
    INSERT INTO ESTADO_CIVIL (TIPO_ESTADO_CIVIL) VALUES ('CASADO');
    INSERT INTO ESTADO_CIVIL (TIPO_ESTADO_CIVIL) VALUES ('DIVORCIADO');
    INSERT INTO ESTADO_CIVIL (TIPO_ESTADO_CIVIL) VALUES ('VIUVO');
    INSERT INTO ESTADO_CIVIL (TIPO_ESTADO_CIVIL) VALUES ('UNIÃO ESTAVEL');

    # GENERO
    INSERT INTO GENERO (TIPO_GENERO) VALUES ('MASCULINO');
    INSERT INTO GENERO (TIPO_GENERO) VALUES ('FEMININO');
   
    # TIPO_PESSOA
    INSERT INTO TIPO_PESSOA (TIPO_PESSOA) VALUES ('Cliente');
    INSERT INTO TIPO_PESSOA (TIPO_PESSOA) VALUES ('Usuário');
    INSERT INTO TIPO_PESSOA (TIPO_PESSOA) VALUES ('Atendente');

SELECT * FROM usuario;