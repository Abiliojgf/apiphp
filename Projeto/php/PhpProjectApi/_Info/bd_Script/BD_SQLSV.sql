 

/* EX SQL SERver */
/* SQL SERver */
CREATE TABLE usuario (
    idUsu INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
    nomeUsu VARCHAR(60) DEFAULT NULL,
    loginUsu VARCHAR(60) DEFAULT NULL,
    senhaUsu VARCHAR(60) DEFAULT NULL,
    emailUsu VARCHAR(60) DEFAULT NULL,
    nivelUsu INT DEFAULT NULL,
    statusUsu INT DEFAULT NULL,
    dataUsu DATE DEFAULT NULL,
    dataUpdateUsu DATE DEFAULT NULL,
    fotoUsu VARCHAR(50) DEFAULT NULL
)
GO

/*'nomeUsu', 'loginUsu', 'senhaUsu', 'emailUsu', 'nivelUsu', 'statusUsu', 'dataUsu', 'dataUpdateUsu', 'fotoUsu'*/

INSERT INTO usuario(nomeUsu, loginUsu, senhaUsu, emailUsu, nivelUsu, statusUsu, dataUsu, dataUpdateUsu, fotoUsu)
 VALUES ('admin','admin','21232f297a57a5a743894a0e4a801fc3','',3,1, GETDATE(), GETDATE(),null) ;
GO