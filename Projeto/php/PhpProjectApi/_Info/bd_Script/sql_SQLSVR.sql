commit;
rollback;
set autocommit=0;
set autocommit=1;
select @@autocommit;

start transaction;

START TRANSACTION;

SELECT IF( NOW() > '2016-04-11 03:07:22', TRUE, FALSE );

SELECT SYSDATETIME()  
    ,SYSDATETIMEOFFSET()  
    ,SYSUTCDATETIME()  
    ,CURRENT_TIMESTAMP  
    ,GETDATE()  
    ,GETUTCDATE(); 

 SELECT CONVERT (datetime2(0), GETDATE(),0) as data,CONVERT (date, GETDATE()) as DataAtual,CONVERT (time(0), SYSDATETIME(),0) as HoraAtual ;  

SELECT CONVERT (date, SYSDATETIMEOFFSET())  
    ,CONVERT (date, SYSUTCDATETIME())  
    ,CONVERT (date, CURRENT_TIMESTAMP)  
    ,CONVERT (date, GETDATE(),3)  
    ,CONVERT (date, GETUTCDATE());  
  
		 SELECT CONVERT (time, SYSDATETIME())  
    ,CONVERT (time, SYSDATETIMEOFFSET())  
    ,CONVERT (time, SYSUTCDATETIME())  
    ,CONVERT (time, CURRENT_TIMESTAMP)  
    ,CONVERT (time, GETDATE(),108)  
    ,CONVERT (time, GETUTCDATE());  

select * from usuario;