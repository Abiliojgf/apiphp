<?php
require_once("../vendor/autoload.php");

//variavel com configurada Principais
$page = "";
$caminhoview = "";
$NamePageView = "";
$caminho = "../Admin/";
$msgText = "";
$msg = Request::Do_GET("msg", "&nbsp; &nbsp; &nbsp;");

require '' . $caminho . 'includes/Class_Padrao_API.php';

/* sessao */
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

use _lib\raelgc\view\Template;
$tpl = new Template("PadraoPage.html");
$tpl->addFile("head", "../Admin/View/Include/head.html");
$tpl->addFile("HRADER_MENU", "../Admin/View/Include/header_menu.html");
$tpl->addFile("FOOTER", "../Admin/View/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../Admin/View/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../Admin/View/Include/script.html");
$tpl->addFile("ModalOpcoes", "../view/partsHTML/modal/ModalOpcoes.html");

$tpl->caminho = $caminho;
$tpl->caminhos = "../Admin/";

/* ConfigHTML */
require '' . $caminho . 'includes/Config.php';

/* valida dizendo que sรณ o nivel 4 pode acessa */
//$nivelR = array(0 => "4", 1 => "5");
require '' . $caminho . 'includes/InfoUserLogado.php';

$tpl->show();
