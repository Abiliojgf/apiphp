<?php
// Configura para que qualquer erro, warning ou notice do PHP seja exibido
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
require 'vendor/autoload.php';

/* CLASS */
$confServer = new ConfigServerPHP();
$conn = new Conexao();
$sql = new Sql(null);
/*Config*/
$conn->setDatabase("apitest");
$tmp = NULL;
/*VAR*/
$arraySQL = null;
$varSQL = null;

if($conn->TestConect()):
    echo $conn->getMsgInfo() . "<br/>";
else:
    echo $conn->getMsgErro() . "<br/>";
endif;
echo $conn->getHost_info() . " <br/>";

/* table */
$varSQL[] = "truncate FUNCOES;";
$varSQL[] = "truncate CARGOS;";

$arraySQL[] = $varSQL[0];
$arraySQL[] = $varSQL[1];
$arraySQL[] = "INSERT INTO FUNCOES (ID_FUNCOES, FUNCAO) VALUES(NULL,'chapa');";
//$arraySQL[] = "UPDATE FUNCOES SET FUNCAO='test' WHERE ID_FUNCOES = 1 ;";
//$arraySQL[] = "DELETE FROM FUNCOES WHERE ID_FUNCOES = 1 ;";
$arraySQL[] = "INSERT INTO FUNCOES (ID_FUNCOES, FUNCAO)  VALUES (null,'CHAPA'),(null,'ATENDEMENTO');";
$arraySQL[] = "INSERT INTO CARGOS (ID_CARGOS, TIPO_CARGO) VALUES "
        . "(null,'Administrador'),(null,'Usuário'),(null,'Atendente'),(null,'SupUSER');";
$arraySQL[] = "UPDATE CARGOS SET TIPO_CARGO='Administradores' WHERE ID_CARGOS = 1 ;";
$arraySQL[] = "UPDATE CARGOS SET TIPO_CARGO='Usuários' WHERE ID_CARGOS = 2 ; ";

$arraySQLl[] = "SELECT O.FUNCAO as nome, O.ID_FUNCOES as id FROM FUNCOES AS O ORDER BY FUNCAO ASC ;";
$arraySQLl[] = "SELECT O.TIPO_CARGO as nome, O.ID_CARGOS as id FROM CARGOS AS O ORDER BY TIPO_CARGO ASC ;";
$sql = $arraySQLl[0];
//$sql = $arraySQLl[1];
$conn->sql = $sql;
//$conn->QuerysTransaction($arraySQL);

echo '' . $conn->getMsgErro();
$result = $conn->montaArrayPesquisa();
echo '' . $conn->getMsgErro();

echo " <br/> ";
?>
<html>
    <head>
        <title>test</title>
    </head>
    <body>
        
        <table border="1">
            
            <thead>
                <tr>
                    <th>id</th>
                    <th>nome</th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach ($result as $row):?>
                <tr>
                    <td><?php echo $row['id']; ?></td>  
                    <td><?php echo $row['nome']; ?></td>  
                </tr>
                <?php endforeach;?>
            </tbody>
            
        </table>

    </body>
</html>