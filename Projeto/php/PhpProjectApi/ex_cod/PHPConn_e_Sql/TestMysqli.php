<?php

ini_set('default_charset', 'UTF-8');
header("Content-Type: " . "text/plain");

//ini_set('default_charset', 'UTF-8');
//header("Content-Type: " . "text/plain");
require 'vendor/autoload.php';

$camposTabelas = array("P.NOME_PRODUTO", "P.TAMANHO_PRODUTO", "P.STATUS_PRODUTO", "P.ID_PRODUTO");
$nomeTabelas = array("P" => "PRODUTO");

$sql = new Sql(null);
$sql->tabela = $nomeTabelas;
$sql->camposTabelas = $camposTabelas;
$sql->ArryasTOMaiusculas = null;
$sql->condicoesTabela = null;
$sql->colunaOrdenada = null; //"nome"
$sql->ordenacao = null; //
$sql->limit = 1000; //
$sql->TOP = null; //
//echo $sql->sqlPesquisar();
//echo '<hr/>';

$conn = new Conexao();
$conn->sql = $sql->sqlPesquisar();
//$result = $conn->montaArrayPesquisa();
//$result = $conn->RsutArrayAssoc();
$result = $conn->RsutArrayAssocII();
if ($conn->getIsOk()):
    echo json_encode($result, JSON_PRETTY_PRINT);
else:
    echo 'erro: ' . $conn->getMsgErro();
endif;
