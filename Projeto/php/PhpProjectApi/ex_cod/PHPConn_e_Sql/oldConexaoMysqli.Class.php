<?php

class ConexaoMysqli {

    private $bancoDeDados;
    private $usuario;
    private $servidor;
    private $senha;
    private $porta;
    private $conexao;
    private $execucao;
    private $erro;
    public $sql;
    public $string;

    function __construct() {
        $configBD = new ConfigBD();
        $this->bancoDeDados = $configBD->getBancoDeDados();
        $this->usuario = $configBD->getUsuario();
        $this->servidor = $configBD->getServidor();
        $this->senha = $configBD->getSenha();
        $this->porta = $configBD->getPorta();
    }

    private function conecta() {
        $this->conexao = new mysqli($this->servidor, $this->usuario, $this->senha, $this->bancoDeDados, $this->porta);
        if ($this->conexao) {
            return $this->conexao;
        } else {
            trigger_error(mysqli_connect_error());
        }
    }

    public function executaQuery() {
        try {
            $this->conecta();
        } catch (Exception $erro) {
            throw new Exception($erro->getMessage());
        }
        $this->conexao->query('SET NAMES utf8');
        $this->conexao->query('SET character_set_connection=utf8');
        $this->conexao->query('SET character_set_client=utf8');
        $this->conexao->query('SET character_set_results=utf8');
        $this->execucao = $this->conexao->query($this->sql);
        if ($this->execucao) {
            $this->desconecta();
            return $this->execucao;
        } else {
            $this->desconecta();
            throw new Exception("Erro ao encontrar a tabela de banco de dados.");
        }
    }

    private function desconecta() {
        $this->conexao->close();
    }

    public function montaArrayPesquisa() {
        $i = 0;
        if (count($this->executaQuery()->fetch_array(MYSQLI_BOTH))) { //Se um ou mais resultados forem retornados...
            $array = $this->executaQuery();
            while ($a = $array->fetch_array(MYSQLI_BOTH)) {
                $arrayDados[$i] = $a;
                $i++;
            }
            return $arrayDados;
        } else {
            return null;
        }
    }

    public function RsutArrayAssoc() {
        $result = $this->executaQuery();
        return mysqli_fetch_array($result, MYSQLI_ASSOC);
    }

    public function RsutArrayAssocII() {
        $i = 0;
        if (count($this->executaQuery()->fetch_array(MYSQLI_BOTH))) { //Se um ou mais resultados forem retornados...
            $array = $this->executaQuery();
            while ($a = $array->fetch_array(MYSQLI_ASSOC)) {
                $arrayDados[$i] = $a;
                $i++;
            }
            return $arrayDados;
        } else {
            return null;
        }
    }

    public function TestConect() {
        $mysqli = new mysqli($this->servidor, $this->usuario, $this->senha, $this->bancoDeDados, $this->porta); /* check connection */
        if (mysqli_connect_errno()) {
            return "Connection failed: " . mysqli_connect_error();
        }
        $mysqli->close();
        return "Connected";
    }

    public function linhasPesquisadas( $paramTipo) {
        $tipo = strtolower($paramTipo);
        if ($tipo == "select") {
            return mysqli_num_rows($this->executaQuery());
        } else {
            return mysqli_affected_rows($this->executaQuery());
        }
    }

    public function getErro() {
        return $this->erro;
    }

    public function commit() {
        $this->sql = "COMMIT";
        return $this->executaQuery();
    }

    public function rollback() {
        $this->sql = "ROLLBACK";
        return $this->executaQuery();
    }

    public function setBancoDeDados($bancoDeDados) {
        $this->bancoDeDados = $bancoDeDados;
    }

    public function GetBancoDeDados()  {
        return $this->bancoDeDados;
    }
}
