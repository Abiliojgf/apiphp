<?php

$msg = "";
$msgErro = "";
$configBD = new ConfigBDClass();
$configBD->setBancoDeDados("apitest");
$database = $configBD->getBancoDeDados();
$user = $configBD->getUsuario();
$server = $configBD->getServidor();
$password = $configBD->getSenha();
$port = $configBD->getPorta();
//$options = null;
$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8');

try {
    $isOk = true;
    $connection = new PDO('mysql:host=' . $server . ';port=' . $port . ';dbname=' . $database . ';charset=utf8', $user, $password, $options);
} catch (PDOException $e) {
    $isOk = false;
    $msgErro = $e->getMessage() . ". ";
}
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
try {
    $msg = "TUDO oK!";
    $num_row = $connection->exec("INSERT INTO FUNCOES (ID_FUNCOES, FUNCAO) VALUES(NULL,'chapa');");
    if(!$num_row) throw new PDOException;
} catch (PDOException $e) {
        $msg = "Error: " . $e->getMessage();
}

$connection = null;

echo "" . $msg;