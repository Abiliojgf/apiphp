<?php

/*  */

//VAR
$varInt = 0;

try {
    $connPDO = new PDO('mysql:host=localhost;port=3306;dbname='.$connConf->getBancoDeDados(), 'root', 'root');
   
    // configura para que qualquer erro que der ocorra a exceção, inclusive erros de comandos SQL (sem isto, não serão notificados erros na consulta)
    $connPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $select = $connPDO->query($sql->sqlPesquisar());
    $varInt = $select->rowCount();

    // Coloca todos os registros dentro de $dados (o parâmetro define que será retornado apenas 1 array associativo, e não um associativo e outro com índices numéricos, para evitar processos desnecessários)
    $dados = $select->fetchAll(PDO::FETCH_ASSOC);
    if (!$varInt) {
        print "Nenhum resultado encontrado na sua consulta!";
    } else {
        echo "<b>Quantidade de registros: " . count($dados) . "</b><br/>";
        foreach ($dados as $row) {
            print "{$row['ID_FUNCOES']} | {$row['FUNCAO']}<br/>";
        }
    }
    $connPDO = null;
} catch (PDOException $error) {
    print $error->getMessage() . "<br/>";
    die();
}

echo " <br/> " . $varInt;