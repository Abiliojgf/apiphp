<?php
//$this->conexao->query('SET NAMES utf8');
//$this->conexao->query('SET character_set_connection=utf8');
//$this->conexao->query('SET character_set_client=utf8');
//$this->conexao->query('SET character_set_results=utf8');
?>

Como posso definir a variável character_set_results de latin1 para uft8?
Eu pensei que seria o suficiente para adicionar a seguinte variável no my.cnf:

default-character-set=utf8
Mas não parece assim:
mysql> SHOW VARIABLES LIKE 'character_set_%';
+--------------------------+----------------------------+
| Variable_name            | Value                      |
+--------------------------+----------------------------+
| character_set_client     | latin1                     |
| character_set_connection | latin1                     |
| character_set_database   | utf8                       |
| character_set_filesystem | binary                     |
| character_set_results    | latin1                     |
| character_set_server     | utf8                       |
| character_set_system     | utf8                       |
| character_sets_dir       | /usr/share/mysql/charsets/ |
+--------------------------+----------------------------+
Alguém tem uma idéia de como eu posso definir character_set_results para utf8?

down vote
O conjunto de caracteres é negociado entre o cliente e o servidor na conexão.

Para evitar isso e forçar o cliente e, portanto, o servidor a usar o conjunto
de caracteres configurado:

[mysqld]
skip-character-set-client-handshake=1
default-character-set=utf8
