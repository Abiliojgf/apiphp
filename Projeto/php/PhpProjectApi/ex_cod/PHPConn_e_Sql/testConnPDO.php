<?php
// Configura para que qualquer erro, warning ou notice do PHP seja exibido
ini_set('display_errors', 1);
error_reporting( E_ALL | E_STRICT );

$host = 'localhost';
$db = 'pykota';
$username = 'postgres';
$password = '123123';

echo ' PDO Connecting to PostgreSQL <br/>';
//$dsn = "pgsql:host=$host;port=5432;dbname=$db;user=$username;password=$password";
$dsn = "pgsql:host=$host;port=5432;dbname=$db";
try {
    // create a PostgreSQL database connection
    $conn = new PDO($dsn,$username,$password,null);
    if ($conn) {
        echo "Connected to the <strong>$db</strong> database successfully!";
    }
} catch (PDOException $e) {
    // report error message
    echo $e->getMessage();
}
unset($conn);
echo '<br/><br/>';

$host = 'localhost';
$db = 'mysql';
$username = 'root';
$password = 'root';

echo 'PDO Connecting to MySQL <br/>';
$dsn = "mysql:host=$host;dbname=$db";
try {
    $conn = new PDO($dsn, $username, $password, array(PDO::ATTR_PERSISTENT => true));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($conn) {
        echo "Connected to the <strong>$db</strong> database successfully!";
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}
unset($conn);