<?php
require '../../vendor/autoload.php';

$connConf = new ConfigBDClass();
$conn = new Conexao;
$connConf->setBancoDeDados("apitest");
$conn->setDatabase("apitest");

$string = $conn->informationConn();
echo $string . " <br/>";
        
echo "<br/>";
$mysqli = new mysqli($connConf->getServidor(), $connConf->getUsuario(), $connConf->getSenha(), $connConf->getBancoDeDados());

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

/* Insert rows */
$mysqli->query("CREATE TABLE Language SELECT * from CountryLanguage");
printf("Affected rows (INSERT): " .  $mysqli->affected_rows);
echo " <br/>";

$mysqli->query("ALTER TABLE Language ADD Status int default 0");

/* update rows */
$mysqli->query("UPDATE Language SET Status=1 WHERE Percentage > 50");
printf("Affected rows (UPDATE): " .  $mysqli->affected_rows);
echo " <br/>";

/* delete rows */
$mysqli->query("DELETE FROM Language WHERE Percentage < 50");
printf("Affected rows (DELETE): " .  $mysqli->affected_rows);
echo " <br/>";

/* select all rows */
$result = $mysqli->query("SELECT CountryCode FROM Language");
printf("Affected rows (SELECT): " .  $mysqli->affected_rows);
echo " <br/>";

/* Delete table Language */
$mysqli->query("DROP TABLE Language");

/* close connection */
$mysqli->close();
