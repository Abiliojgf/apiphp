<?php

/**
 * Description of ConnectionPDO
 *
 * @author AJGF
 */
class ConexaoPDO {

    private $dsn;
    private $database;
    private $user;
    private $server;
    private $password;
    private $port;
    private $connection;
    private $execution;
    private $server_info;
    private $toerror;
    private $msgErro;
    private $msgInfo;
    private $numrows;
    private $isOk;
    private $varInt;
    private $arrayOptions;
    public $sql;
    public $arraySql;

    function __construct() {
        $this->isOk = false;
        $this->msgInfo = "";
        $this->server = "";
        $this->sql = "";
        $this->arraySql = null;
        $this->numrows = 0;
        $configBD = new ConfigBDClass();
        $this->database = $configBD->getBancoDeDados();
        $this->user = $configBD->getUsuario();
        $this->server = $configBD->getServidor();
        $this->password = $configBD->getSenha();
        $this->port = $configBD->getPorta();
        $this->dsn = $configBD->getDsn();
    }

    private function getConnect(): bool {
        $this->isOk = true;
        try {
            $this->arrayOptions = [PDO::ATTR_PERSISTENT => true];
            $this->connection = new PDO($this->dsn, $this->user, $this->password, $this->arrayOptions);
        } catch (PDOException $e) {
            $this->isOk = false;
            $this->msgErro = $e->getMessage();
        }
        return $this->isOk;
    }

    private function disconnects() {
        $this->connection = null;
        unset($this->connection);
    }

    public function executeQuery() {
        $this->isOk = true;
        $this->msgErro = "";
        if (!$this->getConnect()):
            return $this->isOk;
        endif;
        try {
            $this->execution = $this->connection->query($this->sql);
//            $this->numrows = $this->execution->rowCount();
        } catch (PDOException $e) {
            $this->isOk = false;
            $this->msgErro = $e->getMessage() . "-Ocorreu um erro ao tentar executar esta ação, foi gerado um LOG do mesmo, tente novamente mais tarde.";
        } finally {
            $this->disconnects();
        }
        return $this->execution;
    }

    public function updateQuery(): bool {
        $this->isOk = true;
        $this->msgErro = "";
        if (!$this->getConnect()):
            return $this->isOk;
        endif;
        try {
            $this->execution = $this->connection->query($this->sql);
            $this->numrows = $this->execution->rowCount();
            $this->msgInfo = "Affected rows: " . $this->numrows;
        } catch (PDOException $e) {
            $this->isOk = false;
            $this->msgErro = $e->getMessage() . "-Ocorreu um erro ao tentar executar esta ação, foi gerado um LOG do mesmo, tente novamente mais tarde.";
        } finally {
            $this->disconnects();
        }
        return $this->isOk;
    }

    public function updateQueryTransaction(): bool {
        $this->isOk = true;
        $this->msgErro = "";
        if (!$this->getConnect()):
            return $this->isOk;
        endif;
        try {
            //$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->beginTransaction();
            if ($this->arraySql == null):
                $this->numrows = $this->connection->exec($this->sql);
            else:
                foreach ($this->arraySql as $sql):
                    $this->numrows = $this->connection->exec($sql);
                    if($this->numrows == 0):
                        break;
                    endif;
                endforeach;
            endif;
            $this->msgInfo = "Affected rows: " . $this->numrows;
            if($this->numrows > 0):
                $this->connection->commit();
            endif;
        } catch (PDOException $e) {
            $this->connection->rollBack();
            $this->isOk = false;
            $this->msgErro = $e->getMessage() . "-Ocorreu um erro ao tentar executar esta ação, foi gerado um LOG do mesmo, tente novamente mais tarde.";
        } finally {
            $this->disconnects();
        }
        return $this->isOk;
    }

    public function montaArrayPesquisa(): array {
        $i = 0;
        $arrayresult = $this->executeQuery();
        if ($this->isOk):
            while ($a = $arrayresult->fetch()) {
                $arrayDados[$i] = $a;
                $i++;
            }
            return (array) $arrayDados;
        else:
            return (array) null;
        endif;
    }

    public function RsutArrayAssoc(): array {
        $result = $this->executeQuery();
        if ($this->isOk):
            return $result->fetchAll(PDO::FETCH_ASSOC);
        else:
            return (array) null;
        endif;
    }

    public function RsutArrayAssocII(): array {
        $i = 0;
        $arrayresult = $this->executeQuery();
        if ($this->isOk):
            while ($a = $arrayresult->fetchAll(PDO::FETCH_ASSOC)) {
                $arrayDados[$i] = $a;
                $i++;
            }
            return (array) $arrayDados;
        else:
            return (array) null;
        endif;
    }

    public function TestConect(): bool {
        try {
            $this->connection = new PDO($this->dsn, $this->user, $this->password, array(PDO::ATTR_PERSISTENT => false));
        } catch (PDOException $e) {
            $this->isOk = false;
            $this->msgErro = $e->getMessage();
        }
        $this->connection = null;
        return $this->isOk;
    }

    //linhasPesquisadas não implementado
    public function linhasPesquisadas(string $paramTipo): int {
        $this->varInt = 0;
        $tipo = strtolower($paramTipo);
        if ($tipo == "select"):
        else:
        endif;
        return $this->varInt;
    }

    public function setDatabase($database) {
        $this->database = $database;
    }

    public function getServer_info() {
        return $this->server_info;
    }

    public function getToerror() {
        return $this->toerror;
    }

    public function getMsgErro() {
        return $this->msgErro;
    }

    public function getMsgInfo() {
        return $this->msgInfo;
    }

    public function getNumrows() {
        return $this->numrows;
    }

    public function getIsOk() {
        return $this->isOk;
    }

}
