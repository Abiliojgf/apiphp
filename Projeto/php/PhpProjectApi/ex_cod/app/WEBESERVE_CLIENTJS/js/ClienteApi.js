// Capturar o clique no botão #btnClientes...
$(document).on("click", "#btnClientes", function (evt)
{
    // Limpar todos os itens da lista...
    $("#listaClientes").empty();

    // Exibe a mensagem 'Carregndo clientes'
    $("#situacao").html("<center>Buscando clientes no banco de dados (API)...</center>");

    //--jsonp
    // Consumir a API...
    $.ajax({
        type: "GET",
        url: "http://localhost/webserviceslim/clientes",
        timeout: 3000,
        contentType: "application/json; charset=utf-8",
        //dataType: "json",
        success: function (result, jqXHR) {
            try {
                // Interpretando retorno JSON...
                var clientes = jQuery.parseJSON(JSON.stringify(result));//to string
            } catch (e) {
                console.log('erro');
            }
            // Listando cada cliente encontrado na lista...
            $.each(clientes, function (i, cliente) {
                var item = "<li><h2>" + cliente.NOME + "</h2><p><b>Fone.:</b> " + cliente.TELEFONE + "</p><p><b>Email:</b> " + cliente.EMAIL + "</p></li>";
                $("#listaClientes").append(item);
            });
            // Exibir mensagem com total de clientes encontrados...
            $("#situacao").html("<center>Foram encontrado " + clientes.length + " cliente(s)</center>");
        },
        error: function (jqXHR, errorThrown) {
            //Não sei pq ocorre um \ufeff, já verifiquei o coding, coloquei header, alterei o metodo ajax. sei lá
            //Assim funciona como 'esperado'
            if (jqXHR.responseText) {
                //Equivalente ao trecho ",success: function( out ) {"
            }
            console.log(jqXHR);
            console.log(errorThrown);
            // Exibir mensagem de erro, caso aconteça...
            $("#situacao").html("<center>O servidor não conseguiu processar o pedido. Tente novamente mais tarde...</center>");
        }
    });

});
