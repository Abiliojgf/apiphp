$(document).ready(function () {
    var url = "JSONPagination.php";
    var htmlText = "";
    var page = 0;
    var total_cursos = 0;
    var total_cursos_por_page = 0;
    
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: {page: page},
        beforeSend: function () {
//            $("#msg").html('<h3 class="text-info">Carregando...</h3>');
        },
        error: function () {
            $('#msg').html('<h3 class="text-danger">Error: Há allgun problemas com a fonte de dados.</h3>');
        },
        success: function (json) {
            $.each(json.linhas, function (key, value) {
                console.log("total de cursos = " + value.total_cursos + "\ntotal de cursos por page = " + value.total_cursos_por_page);
                total_cursos = value.total_cursos;
                total_cursos_por_page = value.total_cursos_por_page;
            });
            
            $.each(json.cursos, function (key, value) {
                htmlText += '<div class="col-sm-6 col-md-4">';
                htmlText += '<div class="thumbnail">';
                htmlText += '<img src="imagens/produto.jpg" alt="...">';
                htmlText += '<div class="caption text-center">';
                htmlText += '<a href="detalhes.php?id_curso=' + value.id + '">';
                htmlText += '<h3> ' + value.nome + ' </h3>';
                htmlText += '</a>';
                htmlText += '<p><a href="#" class="btn btn-primary" role="button">Comprar</a> </p>';
                htmlText += '</div>';
                htmlText += '</div>';
                htmlText += '</div>';
            });
        }
    });
    $(".cursos").html(htmlText);
});