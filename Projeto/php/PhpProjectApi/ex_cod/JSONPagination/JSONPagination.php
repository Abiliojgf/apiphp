<?php
header("Content-Type: " . "text/plain");
header("Content-Type: application/json");

require '../../vendor/autoload.php';

//class
$config = new ConfigServerPHP();
$conn = new Conexao();
//config
$conn->setDatabase("funildevendas");
$config->Error_ReportingLogin();
$config->Display_errors();

//variavel
$qdd_pg = 0;/* qdd_pg é quantidade */
$Dados = null;
$page = Request::Do_REQUEST("page", 1);//Verificar se está sendo passado na URL a página atual, senao é atribuido a pagina 

//Selecionar todos os cursoS da tabela
$sql_curso = "SELECT * FROM cursos";
$conn->sql = $sql_curso;
$resultado_curso = $conn->executeQuery();

//Contar o total de cursos
$total_cursosI = mysqli_num_rows($resultado_curso) > 0 ? mysqli_num_rows($resultado_curso) : 0;
$qdd_pg = 3;//Seta a quantidade de cursos por paginas
$num_page = ceil($total_cursosI / $qdd_pg);//calcular o número de pagina necessárias para apresentar os cursos
$incio = ($qdd_pg * $page) - $qdd_pg;//Calcular o inicio da visualizacao

//Selecionar os cursos a serem apresentado na página
$sql_cursoS = "SELECT * FROM cursos limit $incio, $qdd_pg";
$conn->sql = $sql_cursoS;
$resultado_cursoS = $conn->montaArrayPesquisa();
$total_cursosII = $conn->linhasPesquisadas("select");

foreach ($resultado_cursoS as $row):
    $Dados['cursos'][] = ["id" => $row["id"], "nome" => $row["nome"]];
endforeach;

$Dados['linhas'][] = array('total_cursos' => $total_cursosI, 'total_cursos_por_page' => $total_cursosII);

//Classes JSon nativa do php utilização
//echo json_encode($Dados, JSON_PRETTY_PRINT);
echo json_encode($Dados, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
//echo '<pre>'.json_encode($Dados,JSON_PRETTY_PRINT).'</pre>';