class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
class="col-xs-1 col-sm-1 col-md-1 col-lg-1"

<div class="content-panel"></div>

<?php
//frm***.html PadraoPageFORM.html FORNhtm.html
require_once("../vendor/autoload.php");

//variavel com configurada Principais
$page = "";
$nomeControle = "";
$NamePageView = "";
$caminho = "../../";
$msgText = "";
$msg = Request::Do_GET("msg", 0);

require '' . $caminho . 'includes/Class_Padrao_API.php';

/* sessao */
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

use _lib\raelgc\view\Template;
$tpl = new Template("PadraoPageFORM.html");
$tpl->addFile("HEAD", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");
//LST
$tpl->addFile("ModalOpcoes", "view/partsHTML/modal/ModalOpcoes.html");

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";

/* ConfigHTML */
require '' . $caminho . 'includes/Config.php';

/* valida dizendo que sรณ o nivel 4 pode acessa */
//$nivelR = array(0 => "4", 1 => "5");
require '' . $caminho . 'includes/InfoUserLogado.php';

//vARIAVEL
$op = Request::Do_GET("op", null);
$disabled = "";

//////////////////////////////////////////////////
$msgText = $MSGobg->MenssagemToPAGE($msg);

////////////////////////////////////////////////
//list
$page = "Lista de " . $NamePageView;

///////////////////////////////////////////////
//forn 1
switch ($op):
    case 0 :
	$page = "Cadastro de " . $NamePageView;
	break;
    case 1 :
	$page = "Alterar de " . $NamePageView;
	break;
    case 2 :
	$disabled = "disabled";
	$page = "Visualizando " . $NamePageView;
	break;
    default :
	header('location: ../index.php');
	break;
endswitch;

////////////////////////////////////////////////////////
//forn 2
$page = $PHTML->OPNamePage($op,$NamePageview);
$disabled = $PHTML->getImputDisabled();
////////////////////////////////////////////////////////
//form 1
if ($op <= 1):
    $tpl->block("BLOCK_BTN");
    $tpl->clear("BLOCK_BTNv");
else:
    $tpl->clear("BLOCK_BTN");
    $tpl->block("BLOCK_BTNv");
endif;
////////////////////////////////////////////////////////
$DadosArray = (string) null;
if ($op == 0):
    $tpl->clear("ModalOpcoes");
    $DadosArray = $dao->Listar();
else:
    $tpl->nomeControle = $nomeControle;
    $tpl->TIPO = 0;
    $DadosArray = $dao->ListarToFone();
endif;
/////////////////////////////////////////////////////////

$tpl->page = $page;
$tpl->msg = $msgText;
$tpl->OP = $op;
//$tpl->disabled = $disabled;
$tpl->show();
