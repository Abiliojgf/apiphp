<?php

ini_set('default_charset', 'UTF-8');

// carrega o carregador PHPMailer
require_once '../Api_lib/phpmailer/phpmailer/';
// carrega o carregador automático do compositor
require '../vendor/autoload.php';

$Nome = NULL;
$Email = NULL;


$message = "Olá {$nome}, redefinimos sua senha.<br /><br />
      <strong>Seu login é</strong>: {$login}<br /><br />
      <strong>Sua Nova Senha é</strong>: {$senhanova}<br /><br />
      <a href=''>Imoveis</a><br /><br />
      Obrigado!<br /><br />
      <br /><br /><br />
      Esta é uma mensagem automática, por favor não responda!";

$phpmailer = new phpmailerUtil();
$phpmailer->I_Remetente_Destinatários($Email, $Nome, 1, null, null);
$phpmailer->IIConfigMsg(true);
$phpmailer->IIIDefineMensagem("Sua nova senha em App CASL", $message);

if ($phpmailer->IVEnviar()):
    echo 'Ok';
else:
    echo 'falhou';
endif;