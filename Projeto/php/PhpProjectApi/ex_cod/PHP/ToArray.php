<h1>Exemplos ¶</h1>

<h3>Exemplo #1 Exemplo da função count()</h3>

<?php
$a[0] = 1;
$a[1] = 3;
$a[2] = 5;
$result = count($a);
echo $result . "<br/>";
// $result == 3

$b[0] = 7;
$b[5] = 9;
$b[10] = 11;
$result = count($b);
echo $result . "<br/>";
// $result == 3

$result = count(null);
echo $result . "<br/>";
// $result == 0

$result = count(false);
echo $result . "<br/>";
// $result == 1
?>

<h3>Exemplo #2 Uso recursivo da função count()</h3>

<?php
$comidas = array('frutas' => array('laranja', 'banana', 'maçã'),
    'veggie' => array('cenoura', 'couve', 'ervilha'));

// contagem recursiva
echo count($comidas, COUNT_RECURSIVE) . "<br/>";
// mostra 8
// contagem normal
echo count($comidas) . "<br/>";
// mostra 2
?>
<br/><br/>
is_array() - Verifica se a variável é um array <br/>
isset() - Informa se a variável foi iniciada <br/>
strlen() - Retorna o tamanho de uma string <br/>
<br/>

<?php
$array_failedConnHost_usEN = array(
    "Connection" => "Connection",
    "failed:" => "failed:",
    "php_network_getaddresses:" => "php_network_getaddresses:",
    "getaddrinfo" => "getaddrinfo",
    "failed:" => "failed:",
    "Este" => "Este",
    "host" => "host",
    "n�o" => "n�o",
    "�" => "�",
    "conhecido." => "conhecido.",
    "10" => ""
);

$array_failedConnHost_ptBR = array(
    "Connection" => "Falha",
    "failed:" => "na conexão:",
    "php_network_getaddresses:" => "php_network_getaddresses:",
    "getaddrinfo" => "getaddrinfo",
    "failed:" => "falhou:",
    "Este" => "Este",
    "host" => "host",
    "n�o" => "não",
    "�" => "é",
    "conhecido." => "conhecido.",
    "" => ""
);

$array_failedConn_usEN = array(
    "Connection" => "Connection",
    "failed" => "failed:",
    "Access" => "Access",
    "denied" => "denied",
    "for" => "for",
    "user" => "user",
    "root" => "'rootadmin'@'localhost'",
    "msgi" => "(using",
    "msgii" => "password:",
    "msgiii" => "YES)"
);

$array_failedConn_ptBR = array(
    "Connection" => "Conexão",
    "failed" => "falhou:",
    "Access" => "Acesso",
    "denied" => "negado",
    "for" => "para",
    "user" => "usuário",
    "root" => "'root'@'localhost'",
    "msgi" => "(usando",
    "msgii" => "senha:",
    "msgiii" => "SIM)"
);

$array_failedConn_ptBR['root'] = $array_failedConn_usEN['root'];
echo $array_failedConn_ptBR['root'];
echo "<br/><br/>";

foreach ($array_failedConn_usEN as $key => $value):
//   foreach ($array as $key => $value):
   echo "{$key} => {$value} <br/>";

//   endforeach;
endforeach;

echo "<br/>";
echo "<br/>";
echo "<pre>";
print_r($array_failedConn_ptBR);
echo "</pre>";
echo "<br/>";
echo "<br/>";

//pegando indice
$arrayN = array(0 => 100, "cor" => "vermelho");
print_r(array_keys($arrayN));
echo "<br/>";


