<?php

/*
 - funções
 
LC_COLLATE - para especificar as regras da localidade para comparação de textos.
LC_CTYPE - para especificar as regras da localidade para classificação/conversão de caracteres.
LC_MONETARY - para especificar a notação monetária de uma localidade.
LC_NUMERIC - para especificar a notação numérica de uma localidade.
LC_TIME - para especificar a notação de data/tempo de uma localidade.
LC_MESSAGES - para especificar o idioma das mensagens de log.
LC_ALL - para especificar a localidade para todas as categorias.
 
 */

setlocale(LC_ALL, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');
$localidade_atual_numeric = setlocale(LC_NUMERIC, '0');
echo 'local configurado: ' . $localidade_atual_numeric . '<br/><br/>';

echo 'Comparando textos <br/>';
// Comparação binária
$comparacao1 = strcmp('índio', 'laranja');
echo " Comparação binária: ". $comparacao1 . "<br/>";

// Comparação de acordo com a localidade
setlocale(LC_COLLATE, 'pt_BR.UTF-8');
$comparacao2 = strcoll('índio', 'laranja');
echo "Comparação de acordo com a localidade: " . $comparacao2 . "<br/>";

$array = array(
    'índio',
    'aluguel',
    'laranja',
    'carro'
);

setlocale(LC_COLLATE, 'pt_BR.UTF-8');
echo "Comparação de acordo com a localidade com array: " . usort($array, 'strcoll') . '<br/>';

//Classificação/conversão de Caracteres
setlocale(LC_CTYPE, 'pt_BR.ISO-8859-1');
$alpha = ctype_alpha('ã');           // Recebe valor "true"
$maiuscula = strtoupper('avião');    // Recebe valor "AVIÃO"
echo 'texto : ' . $maiuscula . '<br/>';
$maiuscula = mb_strtoupper('avião', 'UTF-8');
echo 'texto : ' . $maiuscula . '<br/>';

//Formatação de números
$float = 1500.87;
setlocale(LC_NUMERIC, 'pt_BR.UTF-8');
echo $float . ' <br/>';     // Imprime 1.500,87

$float = 1500.87;

$locale_antigo = setlocale(LC_NUMERIC, '0');
setlocale(LC_NUMERIC, 'C');

echo $float . ' <br/> <br/>';     // Imprime 1500.87

setlocale(LC_NUMERIC, $locale_antigo);

//Formatação Monetária
//De forma similar à definição da notação numérica com LC_NUMERIC, existe a categoria LC_MONETARY para definir a notação monetária de alguma localidade. Por exemplo, no Brasil, utilizamos a notação "R$ x.xxxx,xx". Definindo a localidade nesta categoria, modificamos o comportamento da função money_format:

$float = 1500.87;

setlocale(LC_MONETARY, 'pt_BR.UTF-8');

echo money_format('%n', $float) . ' <br/>';   // Imprime: R$ 1.500,87
echo money_format('%i', $float) . ' <br/><br/>';   // Imprime: BRL 1.500,87
//Observação: a função money_format não está definida em todos os sistemas. Veja uma solução alternativa para money_format.

//Formatação de Data
//Algumas flags usadas pela função strftime dependem da localidade. Por exemplo, a flat "%A" representa o nome do dia da semana. Se definirmos a localidade para a categoria LC_TIME, obtemos o nome correto para as datas, conforme o exemplo:

$time = mktime(0, 0, 0, 9, 26, 2012);

setlocale(LC_TIME, 'pt_BR.UTF-8');
echo strftime('%A, %d de %B de %Y', $time) . ' <br/>'; // Imprime: quarta, 26 de setembro 