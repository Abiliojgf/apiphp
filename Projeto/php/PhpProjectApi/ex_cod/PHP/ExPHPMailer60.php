<?php

// carrega o carregador PHPMailer
require '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../vendor/phpmailer/phpmailer/src/Exception.php';
// carrega o carregador automático do compositor
require '../vendor/autoload.php';

// Importar classes PHPMailer para o namespace global
// Estes devem estar no topo do seu script, não dentro de uma função
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


$nome = NULL;
$Email = NULL;
if ($Email == null) {
    $Email = $confEmail->getEmailDefault();
}
if ($Nome == null) {
    $Nome = $confEmail->getNomeDefault();
}

$confEmail = new ConfigEmailClass();
$mail = new PHPMailer(true); // Passar `true` permite exceções
try {
    // Configurações do servidor
    $mail->SMTPDebug = $confEmail->getSMTPDebug(); // Habilitar saída de depuração detalhada
    $mail->isSMTP(); // Defina mail para usar o SMTP
    $mail->Host = $confEmail->getHost();  // Especificar servidores SMTP principais e de backup
    $mail->SMTPAuth = $confEmail->getSMTPAuth(); // Ativar autenticação SMTP
    $mail->Username = $confEmail->getUsername(); // nome de usuário SMTP
    $mail->Password = $confEmail->getPassword(); // Senha SMTP
    $mail->SMTPSecure = $confEmail->getSMTPSecure(); // Ativar criptografia TLS, `ssl` também aceitou
    $mail->Port = $confEmail->getPort(); // Porta TCP para se conectar a
    // Destinatários
    $mail->setFrom($confEmail->getEmailDefault(), $confEmail->getNomeDefault());
    $mail->addAddress('ajgfqbu@gmail.com', 'Abilio'); // Adicionar um destinatário
    $mail->addAddress('ajgfqbu@gmail.com'); // O nome é opcional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');
    //Anexos
    //$mail->addAttachment('/var/tmp/file.tar.gz'); // Adicionar Anexos
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Nome opcional
    //Conteúdo
    $mail->isHTML($confEmail->getIsHTML()); // Definir formato de email para HTML
    $mail->Subject = 'Aqui está o assunto';
    $mail->Body = 'Este é o corpo da mensagem HTML <b>em negrito!</b>';
    $mail->AltBody = 'Este é o corpo em texto simples para clientes de correio não-HTML';

    $mail->send();
    echo 'mensagem enviada';
} catch (Exception $e) {
    echo 'A mensagem não pôde ser enviada.';
    echo 'Erro do Mailer: ' . $mail->ErrorInfo;
}