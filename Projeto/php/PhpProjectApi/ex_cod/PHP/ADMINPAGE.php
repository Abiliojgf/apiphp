<?php

//variavel com configurada Principais
$caminhoview = "";
$NamePageView = "";
$caminho = "../";
$msgText = "";
$msg = Request::Do_GET("msg", "&nbsp; &nbsp; &nbsp;");

//CLASS API
require '' . $caminho . 'includes/Class_Padrao_API.php';

/* sessao */
$link = $caminho;
require '' . $caminho . 'includes/Validasesao.php';

use _lib\raelgc\view\Template;

$tpl = new Template("../view/index.html");
$tpl->addFile("head", "../view/Include/head.html");
$tpl->addFile("HRADER_MENU", "../view/Include/header_menu.html");
$tpl->addFile("FOOTER", "../view/Include/footer.html");
$tpl->addFile("SCRIPT_UTIL", "../view/Include/script_Util.html");
$tpl->addFile("SCRIPT", "../view/Include/script.html");

$tpl->caminho = $caminho;
$tpl->caminhos = "../../";

/* ConfigHTML */
require '' . $caminho . 'includes/Config.php';

/* valida dizendo que só o nivel 4 pode acessa */
//$nivelR = array(0 => "4", 1 => "5");
require '' . $caminho . 'includes/InfoUserLogado.php';
