<?php

echo '<br/><br/>';
//strstr — Encontra a primeira ocorrencia de uma string
//string strstr ( string $haystack , mixed $needle [, bool $before_needle ] )
//Retorna parte da string haystack a partir da primeira ocorrência de needle até o final de haystack.
$email = 'name@example.com';
$domain = strstr($email, '@');
echo $domain; // prints @example.com

echo '<br/><br/>';

// strrchr — Encontra a ultima ocorrência de um caractere em uma string
$text = "Line 1\nLine 2\nLine 3";
echo '' . $text . '<br/>';
$last = substr(strrchr($text, 10), 1);
echo '' . $last . '<br/>';

//
$findme = 'z';
$mystring1 = 'xyz';
$mystring2 = 'ABC';

$pos1 = stripos($mystring1, $findme);
$pos2 = stripos($mystring2, $findme);

// 'a' certamente não esta em 'xyz'
if ($pos1 === false) {
    echo "A string '$findme' não foi encontrada na string '$mystring1'";
}
echo '<br/>';
// Note o nosso uso de  ===. Simples == não funcionaria como o esperado
// porque a posição de 'a' é 0 (primeiro) caractere.
if ($pos2 !== false) {
    echo "Nós encontramos '$findme' em '$mystring2' na posição $pos2";
}

echo '<br/><br/>';
$mail = "usuario@provedor.com.br";
if (preg_match("#^([\w\.-]+)\@([\w\.-]+)+\.([a-z]{2,6})$#", $mail)) {
    echo "Passou!";
} else {
    echo "Não Passou!";
}

echo '<br/><br/>';
$mail = "usuario@provedor.com.br";
if (strstr($mail, "@")) {
    echo "Passou!";
} else {
    echo "Não Passou!";
}

