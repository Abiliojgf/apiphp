<?php

// carrega o carregador automático do compositor
// Importar classes PHPMailer para o namespace global
require '../Api_lib/phpmailer/phpmailer/PHPMailerAutoload.php';
require '../Config_Server/ConfigEmailClass.Class.php';

$confEmail = new ConfigEmailClass();

$mail = new PHPMailer(true); //PHPMailer 52 
try {
    // Configurações do servidor
    // Define os dados do servidor e tipo de conexão
    // Define que a mensagem será SMTP
    $mail->IsSMTP();
    // Ativar a depuração SMTP  0 = off (para uso em produção) * 1 = mensagens de cliente * 2 = cliente e servidor de mensagens
    $mail->SMTPDebug = $confEmail->getSMTPDebug();
    // Peça saída de depuração HTML-friendly
    $mail->Debugoutput = $confEmail->getDebugoutput();
    $mail->Host = $confEmail->getHost(); // Endereço do servidor SMTP   srv124.prodns.com.br  mail.coopnes.com
    $mail->Port = $confEmail->getPort(); // Definir o número da porta SMTP - provável que seja 25, 465 ou 587
    $mail->SMTPAuth = $confEmail->getSMTPAuth(); // Usa autenticação SMTP? (opcional)
    $mail->SMTPSecure = $confEmail->getSMTPSecure(); /* Protocolo da conexão */
    $mail->Username = $confEmail->getUsername(); // Usuário do servidor SMTP
    $mail->Password = $confEmail->getPassword(); // Senha do servidor SMTP

    $mail->send();
    echo 'mensagem enviada';
} catch (Exception $e) {
    echo 'A mensagem não pôde ser enviada.';
    echo 'Erro do Mailer: ' . $mail->ErrorInfo;
}