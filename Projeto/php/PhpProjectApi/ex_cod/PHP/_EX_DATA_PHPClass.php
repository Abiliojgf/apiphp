<?php

require '../vendor/autoload.php';

$Service = new ConfigServerPHP();
$Service->Default_charset();

//////////////////////////////////////////////////////////////////////////////////////////////
$data1 = new DateTime('2017-07-23');
$data2 = new DateTime('1992-01-31');

$intervalo = $data1->diff($data2);

echo 'EXEMPLO 1 <br/><br/>';
echo "Intervalo é de {$intervalo->y} anos, {$intervalo->m} meses, {$intervalo->d} dias e {$intervalo->h} Horas";
echo '<br/><br/>';

//////////////////////////////////////////////////////////////////////////////////////////////
$dateInicio = new DateTime('2017-05-01 12:59:59');
$dateFim = new DateTime('2017-07-01 11:59:59');

var_dump($dateInicio->diff($dateFim));

echo '<br/><br/>';

$temp = $dateInicio->diff($dateFim);

echo 'EXEMPLO 2 <br/><br/>';
echo "Intervalo é de {$temp->y} anos, {$temp->m} meses, {$temp->d} dias e {$temp->h} Horas";
echo '<br/><br/>';

//////////////////////////////////////////////////////////////////////////////////////////////
$datetime1 = date_create('2009-10-14 15:00');
$datetime2 = date_create('2009-10-13 18:00');
$interval = date_diff($datetime2, $datetime1);

$dias = $interval->format('%d');
$minutos = $interval->format('%I');
$horas = $interval->format('%H');
$horas = $horas + ($dias * 24);

$horas = str_pad($horas, 2, "0", STR_PAD_LEFT);

echo 'EXEMPLO 3 <br/><br/>';
echo "$horas:$minutos";
echo '<br/><br/>';

//////////////////////////////////////////////////////////////////////////////////////////////
$time = new DateTime('2009-10-14 15:00');
$diff = $time->diff(new DateTime('2009-10-14 13:00'));

list( $days, $minutes, $hours ) = array($diff->d, $diff->m, $diff->h);

$hours += $days * 24;

echo 'EXEMPLO 4 <br/><br/>';
var_dump(str_pad($hours, 2, '0', STR_PAD_LEFT));
echo '<br/><br/>';

//////////////////////////////////////////////////////////////////////////////////////

$dataN1 = '2017-07-23 12:30:00';
$dataN2 = '2017-07-23 14:00:00';

$unix_data1 = strtotime($dataN1);
$unix_data2 = strtotime($dataN2);

$nHoras = ($unix_data2 - $unix_data1) / 3600;
$nMinutos = (($unix_data2 - $unix_data1) % 3600) / 60;

echo 'EXEMPLO 5 <br/><br/>';
printf('%02d:%02d', $nHoras, $nMinutos);
