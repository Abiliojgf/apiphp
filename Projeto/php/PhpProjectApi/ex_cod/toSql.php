<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of toSql
 *
 * @author AJGF
 */
class toSql {

    public $tabela = "";
    public $dados;
    public $valoresSql;
    public $instrucaoSql;
    public $camposTabelas;
    public $camposPrimary;
    public $condicoesTabela;
    public $ordenacao;
    public $colunaOrdenada;
    public $limit;
    public $TOP;
    public $ArryasTOMaiusculas;

    /** M�todo construtor
     * @method __construct
     * @param string $tabela
     * @return $this->tabela
     */
    public function __construct($tabela) { // construtor, nome da tabela como parametro
        $this->tabela = $tabela;
        $this->ArryasTOMaiusculas = FALSE;
    }

    private function ArryasSQLTOMaiusculas() {
        $PArrays = new PreparaArrays();
        $qdd = $qtdTabelas = count($this->tabela);
        if ($qdd > 1):
            if (isset($this->tabela) || $this->tabela != null):
                $arraytabela = $PArrays->ArryasTOMaiusculas($this->tabela);
                $this->tabela = $arraytabela;
            endif;
        endif;
        if (isset($this->camposTabelas) || $this->camposTabelas != null):
            $arrayColunas = $PArrays->ArryasTOMaiusculas($this->camposTabelas);
            $this->camposTabelas = $arrayColunas;
        endif;
    }

    public function sqlInserir() {
        if (isset($this->ArryasTOMaiusculas) && $this->ArryasTOMaiusculas == TRUE)
            $this->ArryasSQLTOMaiusculas();
        $coluna = "";
        $tamanhoArray = count($this->dados);
        $interacoes = 1;
        foreach ($this->dados as $valor) {
            if ($interacoes < $tamanhoArray) {
                if ($valor == '' || $valor == null):
                    $this->valoresSql .= "NULL,";
                else:
                    $this->valoresSql .= "'" . $valor . "',";
                endif;
            } else {
                if ($valor == '' || $valor == null):
                    $this->valoresSql .= "NULL";
                else:
                    $this->valoresSql .= "'" . $valor . "'";
                endif;
            }
            $interacoes++;
        }
        if (isset($this->camposTabelas) || $this->camposTabelas != NULL) {
            $tamanhoTabela = count($this->camposTabelas);
            $interacoes = 0;
            foreach ($this->camposTabelas as $campo) {
                if ($interacoes < $tamanhoTabela - 1) {
                    $coluna .= "" . $campo . ", ";
                } else {
                    $coluna .= "" . $campo . "";
                }
                $interacoes++;
            }
        }
        $this->instrucaoSql = "INSERT INTO {$this->tabela} ({$coluna}) VALUES({$this->valoresSql})";
        return $this->instrucaoSql;
    }

    public function sqlInserirMultiplos($dadosMultiplos) {
        if (isset($this->ArryasTOMaiusculas) && $this->ArryasTOMaiusculas == TRUE)
            $this->ArryasSQLTOMaiusculas();
        $coluna = "";
        $tamanhoArrayMultiplos = count($dadosMultiplos);
        $interacoesII = 0;
        foreach ($dadosMultiplos as $valorMultiplos) {
            $interacoes = 0;
            if ($interacoesII < $tamanhoArrayMultiplos - 1) {
                $this->valoresSql .= '(';
                $tamanhovalorMultiplos = count($valorMultiplos);
                foreach ($valorMultiplos as $valor) {
                    if ($interacoes < $tamanhovalorMultiplos - 1) {
                        if ($valor == '' || $valor == null):
                            $this->valoresSql .= "NULL,";
                        else:
                            $this->valoresSql .= "'" . $valor . "',";
                        endif;
                    } else {
                        if ($valor == '' || $valor == null):
                            $this->valoresSql .= "NULL";
                        else:
                            $this->valoresSql .= "'" . $valor . "'";
                        endif;
                    }
                    $interacoes++;
                }
                $this->valoresSql .= '),';
            } else {
                $interacoes = 0;
                $this->valoresSql .= '(';
                $tamanhovalorMultiplos = count($valorMultiplos);
                foreach ($valorMultiplos as $valor) {
                    if ($interacoes < $tamanhovalorMultiplos - 1) {
                        if ($valor == '' || $valor == null):
                            $this->valoresSql .= "NULL,";
                        else:
                            $this->valoresSql .= "'" . $valor . "',";
                        endif;
                    } else {
                        if ($valor == '' || $valor == null):
                            $this->valoresSql .= "NULL";
                        else:
                            $this->valoresSql .= "'" . $valor . "'";
                        endif;
                    }
                    $interacoes++;
                }
                $this->valoresSql .= ');';
            }
            $interacoesII++;
        }
        if (isset($this->camposTabelas) || $this->camposTabelas != NULL) {
            $tamanhoTabela = count($this->camposTabelas);
            $interacoes = 0;
            foreach ($this->camposTabelas as $campo) {
                if ($interacoes < $tamanhoTabela - 1) {
                    $coluna .= "" . $campo . ", ";
                } else {
                    $coluna .= "" . $campo . "";
                }
                $interacoes++;
            }
        }
        $this->instrucaoSql = "INSERT INTO {$this->tabela} ({$coluna}) VALUES {$this->valoresSql}";
        return $this->instrucaoSql;
    }

    public function sqlAtualizar($where = NULL) {
        if (isset($this->ArryasTOMaiusculas) && $this->ArryasTOMaiusculas == TRUE)
            $this->ArryasSQLTOMaiusculas();
        $tamanhoTabela = count($this->camposTabelas);
        $interacoes = 0;
        foreach ($this->camposTabelas as $campo) {
            if ($interacoes < $tamanhoTabela - 1) {
                $this->valoresSql .= $campo . "='" . $this->dados[$interacoes] . "', ";
            } else {
                $this->valoresSql .= $campo . "='" . $this->dados[$interacoes] . "' ";
            }
            $interacoes++;
        }
        if ($where) {
            $this->instrucaoSql = "UPDATE  " . $this->tabela . " SET " . $this->valoresSql . " WHERE " . $where;
        } else {
            $this->instrucaoSql = "UPDATE  " . $this->tabela . " SET " . $this->valoresSql;
        }
        return $this->instrucaoSql;
    }

    public function sqlAtualizarMultiplos($dadosMultiplos, $where = NULL) {
        if (isset($this->ArryasTOMaiusculas) && $this->ArryasTOMaiusculas == TRUE):
            $this->ArryasSQLTOMaiusculas();
        endif;
        $tamanhoTabela = count($this->camposTabelas);
        $tamanhoDados = count($dadosMultiplos);
        $tamanhoWhere = count($where);
        $interacoes = 0;
        $interacoesDados = 0;
        $interacoesWHERE = 0;
        foreach ($dadosMultiplos as $dadoarray) :
            $this->valoresSql = "";
            $interacoes = 0;
            foreach ($dadoarray as $dado):
                foreach ($this->camposTabelas as $campo):
                    if ($interacoes < $tamanhoTabela - 1):
                        $this->valoresSql .= $campo . "='" . $dado . "', ";
                    else:
                        $this->valoresSql .= $campo . "='" . $dado . "' ";
                    endif;
                    $interacoes++;
                endforeach;
            endforeach;
            if ($where):
                if ($tamanhoWhere == $tamanhoDados):
                    $this->instrucaoSql .= "UPDATE  " . $this->tabela . " SET " . $this->valoresSql . " WHERE " . $this->camposPrimary . " = " . $where[$interacoesDados][$interacoesWHERE] . " ;";
                else:
                    $this->instrucaoSql .= "UPDATE  " . $this->tabela . " SET " . $this->valoresSql . " WHERE " . $this->camposPrimary . " = " . $where[0] . " ;";
                endif;
            else:
                $this->instrucaoSql .= "UPDATE  " . $this->tabela . " SET " . $this->valoresSql . " ;";
            endif;
            $interacoesDados++;
        endforeach;
        return $this->instrucaoSql;
    }

    public function sqlexcluir() {
        if (isset($this->ArryasTOMaiusculas) && $this->ArryasTOMaiusculas == TRUE)
            $this->ArryasSQLTOMaiusculas();
        $this->instrucaoSql = "DELETE FROM " . $this->tabela;
        if ($this->condicoesTabela != null) {
            $this->instrucaoSql .= " WHERE ";
            foreach ($this->condicoesTabela as $condicoes) {
                $this->instrucaoSql .= $condicoes . " ";
            }
        }
        return $this->instrucaoSql;
    }

    public function sqlPesquisar() {
        if (isset($this->ArryasTOMaiusculas) && $this->ArryasTOMaiusculas == TRUE)
            $this->ArryasSQLTOMaiusculas();
        $this->instrucaoSql = "SELECT ";
        if (isset($this->TOP) && $this->TOP >= 1) {
            $this->instrucaoSql = "SELECT TOP " . $this->TOP . " ";
        }
        if (isset($this->camposTabelas)) {
            $qtdCampos = count($this->camposTabelas);
            $interacoesCampos = 1;
            foreach ($this->camposTabelas as $campos) {
                if ($interacoesCampos < $qtdCampos) {
                    $this->instrucaoSql .= $campos . ", ";
                } else {
                    $this->instrucaoSql .= $campos . " FROM ";
                }
                $interacoesCampos++;
            }
        } else {
            $this->instrucaoSql .= "* FROM ";
        }
        $qtdTabelas = count($this->tabela);
        $interacoesTabelas = 1;
        foreach ($this->tabela as $apelido => $nomeTb) {
            if ($interacoesTabelas < $qtdTabelas) {
                $this->instrucaoSql .= $nomeTb . " AS " . $apelido . ", ";
            } else {
                $this->instrucaoSql .= $nomeTb . " AS " . $apelido;
            }
            $interacoesTabelas++;
        }
        if ($this->condicoesTabela != null) {
            $qtdcondicoesTabela = count($this->condicoesTabela);
            $interacoescondicoesTabela = 1;
            $this->instrucaoSql .= " WHERE ";
            foreach ($this->condicoesTabela as $condicoes) {
                if ($interacoescondicoesTabela < $qtdcondicoesTabela):
                    $this->instrucaoSql .= $condicoes . " AND ";
                else:
                    $this->instrucaoSql .= $condicoes . " ";
                endif;
                $interacoescondicoesTabela++;
            }
        }
        if ($this->colunaOrdenada != null) {
            if ($this->ordenacao == "DESC") {
                $this->instrucaoSql .= " ORDER BY " . $this->colunaOrdenada . " DESC ";
            } elseif ($this->ordenacao == "ASC") {
                $this->instrucaoSql .= " ORDER BY " . $this->colunaOrdenada . " ASC ";
            }
        }
        if ($this->limit != null) {
            $this->instrucaoSql .= " LIMIT " . $this->limit . "";
        }
        return $this->instrucaoSql;
    }
}
