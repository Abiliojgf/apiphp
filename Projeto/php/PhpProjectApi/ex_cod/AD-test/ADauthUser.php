<?php

/**
 * Description of ADauthUser
 *  Função de validação no AD via protocolo LDAP
 * como usar:
 * valida_ldap("servidor", "domíniousuário", "senha");
 * @author abilio.jose
 */
class ADauthUser {

    private $ldap_server;
    private $ldap_domain;
    private $ldap_port;
    private $auth_user;
    private $auth_pass;
    private $auth_msg;

    public function __construct() {
//        $this->ldap_server = "192.168.0.250";
        $this->ldap_server = "192.168.0.252";
        $this->ldap_domain = "@AJGFAD.LOCAL";
        $this->ldap_port = "389";
    }

    function Authenticate_AD($usr, $pwd) {
        $this->auth_user = $usr;
        $this->auth_pass = $pwd;
        $isOk = FALSE;
        // Tenta se conectar com o servidor
        if (!($connect = @ldap_connect($this->ldap_server))) {
            $this->auth_msg = "Não foi possível conexão com Active Directoy";
            $isOk = FALSE;
        }
        // Tenta autenticar no servidor
        if (!($bind = @ldap_bind($connect, $this->auth_user, $this->auth_pass))) {
            // se não validar retorna false
            $this->auth_msg = "Não foi possível pesquisa no AD.";
            $isOk = FALSE;
        } else {
            // se validar retorna true
            $isOk = TRUE;
        }
        return $isOk;
    }

}
