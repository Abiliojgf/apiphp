<?php
ini_set('default_charset', 'UTF-8');
//json
header("Content-Type: " . "text/plain");
header("Content-Type: application/json");

//require '../vendor/autoload.php';

//VERIAVEIS
$json = [];

//VERIAVEIS de requisições url

//DADOS
$json['s'][] = ["id" => 1, "text" => "A"];
$json['s'][] = ["id" => 2, "text" => "B"];
$json['s'][] = ["id" => 3, "text" => "C"];

$json['r'][] = ["id" => 1, "text" => "D"];
$json['r'][] = ["id" => 2, "text" => "E"];
$json['r'][] = ["id" => 3, "text" => "F"];


//Classes JSon nativa do php utilização
echo json_encode($json);
