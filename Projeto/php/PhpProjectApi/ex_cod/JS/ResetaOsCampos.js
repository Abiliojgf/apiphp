
//No seu código você disse que as variáveis se referem aos campos,
//mas na verdade eles se referem apenas aos valores, o mais correto
//seria usar algo como var [SUA VARIAVEL] = $("[SEU SELETOR]");, exemplo:

var checkbox = $('input:checkbox[name^=check]:checked');
var categoriaVeiculo = $("#codCategoriaVeiculo");
var descricaoVeiculo = $("#descricaoVeiculo");
var placaVeiculo = $("#placaVeiculo");
var chassiVeiculo = $("#chassiVeiculo");
var anoFabricacaoVeiculo = $("#anoFabricacaoVeiculo");
var ativo = $("input:radio[name='ativo']");
var value = ativo.filter(":checked");
var codPessoa = $("#codigoPessoa");
//Para pegar o valor da variável codPessoa, chame assim:

console.log(codPessoa.val());
//ou
alert(codPessoa.val());
//Para limpar o campo você pode usar, algo como .val(""), exemplo:
codPessoa.val("");

//------------------------------------------------------------------------------
//Resetando o form via jQuery

$('#idform').each (function(){
  this.reset();
});