/*
 Em outubro de 2010 escrevi um artigo aqui mesmo no Tableless sobre a associação dinâmica
 de eventos.Na época, reinava uma confusão sobre quais métodos utilizar e
 quando utilizá - los.Eram três as opções: bind(), live() e delegate().
 Com o lançamento da versão 1.7 do jQuery, dois métodos definitivos
 (assim espero) devem acabar com a confusão em torno da associação de eventos:
 os métodos on() e off().

 Eventos diretos
 A associação direta de eventos ocorre quando o seletor (ou escopo) é
 omitido nos parâmetros do on().Por exemplo:
 */

function exibeMenu(e) {
    e.preventDefault();
    $('#menu').show();
}

$('#lnk-menu').on('click', exibeMenu);

/*
 No código acima, o elemento com id #lnk-menu recebe uma associação
 no evento click para executar a função exibeMenu(). A associação ocorre
 de forma direta, ou seja, quando o on() é executado o evento passa
 a estar associado ao elemento, antes mesmo do clique ocorrer.

 Essa opção equivale ao bind() e deve ser utilizada em casos de elementos únicos
 ou com poucos elementos, para evitar problemas de performance.prototype
 */

/*
 Removendo associações de eventos diretos
 Remover associações de eventos diretos é simples:
 */

$('#lnk-menu').off();

/*
 Para remover eventos específicos, mantendo qualquer outra associação,
 basta informar o nome do evento:
 */

$('#lnk-menu').off('click');
//----------------------------------------------------------------------------

//A solução moderna para desativar um link é somente CSS: (para Browsers modernos)
$('#minhaID').css("pointer-events", "none");

// e para re-activar basta usar:
$('#minhaID').css("pointer-events", "");

// Demo - Pode usar tambem assim:
$('#minhaID').attr("href", "");
// Ou ainda:
$('#minhaID').removeAttr('href');

//note que esta opcção remove algumas caracteristicas importantes do elemento como focus.
/*
 dependendo da utilização que quiser dar.
 Nota: O Chrome tem uma discussão aberta sobre a possibilidade de deixar
 de suportar pointer - events.
 */
