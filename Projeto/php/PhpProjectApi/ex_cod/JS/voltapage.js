/*

history.back();

---------------------------------------------------------------------
possibilidades para voltar para a página anterior:

history.go(-1);
window.open(document.referrer,'_self');
location.href = document.referrer;
Exemplos para teste:

<button onclick="history.go(-1);">Voltar 1</button>

<button onclick="location.href = document.referrer;">Voltar 2</button>

<button onclick="window.open(document.referrer,'_self');">Voltar 3</button>

---------------------------------------------------------------------
VOLTAR PARA PÁGINA ANTERIOR UTILIZANDO APENAS O JAVASCRIPT
Roger Takemiya 19 de maio de 2017 Sem comentários
Como voltar para página anterior utilizando apenas o Javascript.

utilizando javascript para Voltar para página anterior
Para fazer isso iremos utilizar window.history.back() que dará o comando para o navegador voltar uma página.

Exemplo com o código
<a title="Voltar" onclick="voltar()">Voltar página anterior</a>

<script>
function voltar() {
    window.history.back();
}
</script>
Você também pode utilizar history.go para avançar uma página com o parâmetro para 1 positivo.

history.go(1)
Ou também para voltar uma página

history.go(-1)
Utilizando a função history.go também é possível voltar mais de uma página, segue os exemplos:

history.go(-2); // volta 1 página
history.go(-3); // 2 páginas
history.go(-4); // 3 páginas e assim por diante..
document.referrer
Uma segunda forma de fazer isso é utilizando document.referrer, que faz referencia a URL da página anterior.

<a title="Voltar uma página" onclick="location.href = document.referrer;">Voltar uma página</a>
E utilizamos o location.href para fazer o redirecionamento para página anterio
