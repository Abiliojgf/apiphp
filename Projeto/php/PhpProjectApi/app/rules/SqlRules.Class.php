<?php

/**
 * Description of SqlRules
 *
 * @author abilio.jose
 */
class SqlRules {

    private $stnSQL;
    private $ClassSQL;

    function __construct() {
        $this->ClassSQL = new Sql(null);
        $this->stnSQL = null;
    }

    function getStnSQL(): string {
        return (string) $this->stnSQL;
    }

    // -- LIST HOME
    public function sqlHome($conditions): string {
        return $this->stnSQL;
    }

}
