<?php

class ConfigBDClass {

    private $bancoDeDados;
    private $usuario;
    private $servidor;
    private $senha;
    private $porta;
    private $charset;
    private $prefixoBD;
    private $drivers;
    private $dsn;
    private $options;
    private $optDB;

    function __construct() {
        $this->prefixoBD = "";
        $this->charset = "utf8";
        $this->drivers = "mysql";// drivers - "mysql"  "pgsql" "sqlite" "sqlsrv"
//    ------------------usado para testes local------------------
        $this->servidor = "localhost";
        $this->bancoDeDados = $this->prefixoBD . "apiphp";
        $this->usuario = "root";
        $this->senha = "root";
        $this->porta = "3306";
        $this->optDB = 0;
//  ------------------ usado POSTGRE para PRODUÇAO------------------
//        $this->servidor = "";
//        $this->bancoDeDados = $this->prefixoBD . "api";
//        $this->usuario = "";
//        $this->senha = "";
//        $this->porta = "";
//        $this->optDB = 1;
//  -----------------------------------------------------------------
        $this->dsn = "".$this->drivers.":host=".$this->servidor.";port=".$this->porta.";dbname=".$this->bancoDeDados."";
        $this->options = array(PDO::ATTR_PERSISTENT => true);
    }

    public function getBancoDeDados() {
        return $this->bancoDeDados;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getServidor() {
        return $this->servidor;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function getPorta() {
        return $this->porta;
    }

    public function getPrefixoBD() {
        return $this->prefixoBD;
    }

    public function getDrivers() {
        return $this->drivers;
    }

    public function getDsn() {
        return $this->dsn;
    }

    public function getOptions() {
        return $this->options;
    }
    
    public function getOptDB() {
        return $this->optDB;
    }

    public function setBancoDeDados(string $parambancoDeDados) {
        $this->bancoDeDados = $parambancoDeDados;
    }

    public function setOptions($options) {
        $this->options = $options;
    }

    public function setOptDB($optDB) {
        $this->optDB = $optDB;
    }

}
