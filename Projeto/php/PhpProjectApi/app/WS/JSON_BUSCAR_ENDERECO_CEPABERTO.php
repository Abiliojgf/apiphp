<?php

require '../vendor/autoload.php';

//Classes de utilização
$service = new ConfigServerPHP();
$service->Default_charset();
$string = new ToString(null);
$Dao = new DaoEndereco();
$objE = new Endereco();

//VERIAVEIS
$encoded = NULL;
$json = NULL;
$Dados = null;

$CEP = Request::Do_POST("cep", NULL);
//$CEP = Request::Do_POST("cep", "75064040");

/* CEP DE SP 01001000 */
//$ParamURL = "lat=-16.279900&lng=-48.977175";
//$ParamURL = "estado=GO&cidade=Anapolis";
$ParamURL = "cep=" . $CEP . "";

if ($CEP != null):
   $objE = $Dao->PegarOBJpeloCEP($CEP);
   if ($objE->getId() != 0):
      $Dados["Endereco"][] = array("id" => $objE->getId(), "cep" => $objE->getCep(), "logradouro" => $objE->getLogradouro(), "bairro" => $objE->getBairro(),
          "cidade" => $objE->getCidade(), "uf" => $objE->getUf());
   else:
      // O Curl irá fazer uma requisição para a API do CEPABERTO.com
      //e irá receber o JSON com as informações.
      $token = '8d86914faad3418e68b62174cfa6911b';
      $url = 'http://www.cepaberto.com/api/v2/ceps.json?' . $ParamURL . '';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Token token="' . $token . '"'));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $json = curl_exec($ch);
      //Faremos o PHP interpretar e reconhecer o JSON que
      //recebemos da API do CEPABERTO.
      $encoded = json_decode($json);
      $Dados["Endereco"][] = array("id" => 0
          , "cep" => $encoded->{'cep'}
          , "logradouro" => $encoded->{'logradouro'}
          , "bairro" => $encoded->{'bairro'}
          , "cidade" => $encoded->{'cidade'}
          , "uf" => $encoded->{'estado'}
      );
   endif;
endif;
/*
  "altitude","bairro","cep","latitude","longitude","logradouro","cidade","ddd","ibge","estado"
 */
//echo json_encode($Dados, JSON_PRETTY_PRINT);
//echo '<pre>' . json_encode($Dados, JSON_PRETTY_PRINT) . '</pre>';
echo json_encode($Dados, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
