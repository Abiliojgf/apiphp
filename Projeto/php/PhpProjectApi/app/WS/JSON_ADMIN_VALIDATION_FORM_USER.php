<?php

require '../vendor/autoload.php';

//Classes de utilização
$Data = new Data();
$String = new ToString(NULL);
$daoUsu = new DaoUsuario();

//vERIAVEIS
$Dados = null;
$IsOKL = false;
$IsOKE = false;
$login = Request::Do_POST("login", null);
$email = Request::Do_POST("email", null);

if ($login != null):
   if ($daoUsu->fucaoVerificarDefull(array("U.LOGIN_USUARIO = '{$login}'"))):
      $IsOKL = TRUE;
   else:
      $IsOKL = FALSE;
   endif;
endif;

if ($email != null):
   if ($daoUsu->fucaoVerificarDefull(array("U.EMAIL_USUARIO = '{$email}'"))):
      $IsOKE = TRUE;
   else:
      $IsOKE = FALSE;
   endif;
endif;

$Dados["Result"][] = array("ResultL" => $IsOKL, "ResultE" => $IsOKE);

echo json_encode($Dados, JSON_PRETTY_PRINT);
