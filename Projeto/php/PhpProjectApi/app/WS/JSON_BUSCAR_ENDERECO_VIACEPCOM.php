<?php

require_once '../../config.php';

//Classes de utilização
$service = new ConfigServerPHP();
$service->Default_charset();
$string = new ToString(null);
$Dao = new DaoEndereco();
$objE = new Endereco();

//vERIAVEIS
$Dados = null;
//$CEP = Request::Do_POST("cep", NULL);
$CEP = Request::Do_POST("cep", "75064040"); //01001000

if ($CEP != null):

   $objE = $Dao->PegarOBJpeloCEP($CEP);

   if ($objE->getId() != 0):

      $Dados["Endereco"][] = array("id" => $objE->getId(), "cep" => $objE->getCep(), "logradouro" => $objE->getLogradouro(), "bairro" => $objE->getBairro(),
          "cidade" => $objE->getCidade(), "uf" => $objE->getUf());

   else:
      // O Curl irá fazer uma requisição para a API do viacep.com
      //e irá receber o JSON com as informações.
      //$curl = curl_init("http://viacep.com.br/ws/" . $CEP . "/json/");
      $curl = curl_init("http://viacep.com.br/ws/" . $CEP . "/json/unicode/");
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $json = curl_exec($curl);
      curl_close($curl);

      //Faremos o PHP interpretar e reconhecer o JSON que recebemos da API do VIACEP.
      $encoded = json_decode($json);

      $Dados["Endereco"][] = array("id" => 0, "cep" => $encoded->{'cep'}, "logradouro" => $encoded->{'logradouro'}, "bairro" => $encoded->{'bairro'},
          "cidade" => $encoded->{'localidade'}, "uf" => $encoded->{'uf'});
   endif;

endif;

//echo json_encode($Dados, JSON_PRETTY_PRINT);
//echo json_encode($Dados, JSON_PRETTY_PRINT);
echo json_encode($Dados, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
