<?php

require '../vendor/autoload.php';

//Classes de utilização
$service = new ConfigServerPHP();
$service->Default_charset();
$string = new ToString(null);
$Dao = new DaoEndereco();
$objE = new Endereco();

//VERIAVEIS
$Dados = null;
$ToString = new ToString();
$key = "f0fbc3123bd7497492c25922170909";
$city = 'Anápolis';
$url = "http://api.apixu.com/v1/current.json?key=$key&q=$city&=";
//$url = "http://api.apixu.com/v1/current.json?key=$key&q=paris&=";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$json_output = curl_exec($ch);
$Dados = json_decode($json_output);

$RESU = array(
    "City" => $Dados->location->name,
    "Region" => $Dados->location->region,
    "Country" => $Dados->location->country,
    "tz_id" => $ToString->TrocarCaracte("\/", "/", $Dados->location->tz_id),
    "Lat" => $Dados->location->lat,
    "Lon" => $Dados->location->lon,
    "Temperature_C" => $Dados->current->temp_c,
    "feels_like_C" => $Dados->current->feelslike_c,
    "temperature_F" => $Dados->current->temp_f,
    "feels_like_F" => $Dados->current->feelslike_f,
    "conditionIMG" => "<img src='" . $ToString->TrocarCaracte("\/", "/", $Dados->current->condition->icon) . "'>",
    "mph" => $Dados->current->wind_mph,
    "kph" => $Dados->current->wind_kph,
    "wd" => $Dados->current->wind_degree . " &deg " . $Dados->current->wind_dir . "",
    "Humidity" => $Dados->current->humidity,
    "last_updated" => $Dados->current->last_updated,
);

//echo json_encode($json_output, JSON_PRETTY_PRINT);
echo '<pre>' . json_encode($RESU, JSON_PRETTY_PRINT) . '</pre>';
